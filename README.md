PrimeArch

1. get a PrimeArch prepared iso:
    - https://drive.google.com/file/d/1C3Bev7PGR8IqnS5oXHHlo9QiUKVXEYPv/view?usp=sharing
    - sha512sum: 745a6f993be6480897ccefac5d5b1c4c62c9dc32171d95e75916eca5caaf06d5ba549b798d45a26aab2541ada7967e7d351350e64d981095823c6450d495f089

2. boot the iso
3. start the script with "win + i" 
4. folow the instructions



to this installer related projekts:
* https://gitlab.com/dikiy4eburator/archiso
* https://gitlab.com/dikiy4eburator/dotfiles.git
* https://gitlab.com/dikiy4eburator/i3-config
* https://gitlab.com/dikiy4eburator/qtile-config
