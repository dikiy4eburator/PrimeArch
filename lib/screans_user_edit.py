from time import sleep

from .e_ascii import (
    draw_text_middle,
    draw_line,
    draw_line_middle,
    draw_noblock_input,
    draw_noblock_input_middle,
    draw_noblock_input_obscure_middle,
    draw_noblock_menu_middle,
)

from .lib_objects import (
    add_decorations
)

def screan_edit_user_name(engine, LINE, SCREAN_CENTER_X, GOLDEN_CUT_Y, version):

    add_decorations(engine, version, LINE, SCREAN_CENTER_X)

    # BODY
    engine.add_drawer(
        draw_noblock_input_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, GOLDEN_CUT_Y + 3]
        )
    )

    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, GOLDEN_CUT_Y],
            text="Please enter new user name"
        )
    )
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, GOLDEN_CUT_Y + 1],
            orientation="h",
            lengh=51,
            char="─"
        )
    )


def screan_edit_user_password(engine, LINE, SCREAN_CENTER_X, GOLDEN_CUT_Y, user_obj, version):

    add_decorations(engine, version, LINE, SCREAN_CENTER_X)

    # BODY
    engine.add_drawer(
        draw_noblock_input_obscure_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, GOLDEN_CUT_Y + 3]
        )
    )

    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, GOLDEN_CUT_Y],
            text="Please enter the password for user " + f"{user_obj.name}"
        )
    )
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, GOLDEN_CUT_Y + 1],
            orientation="h",
            lengh=51,
            char="─"
        )
    )


def screan_confirm_user_password(engine, LINE, SCREAN_CENTER_X, GOLDEN_CUT_Y, user_obj, version):

    add_decorations(engine, version, LINE, SCREAN_CENTER_X)

    # BODY
    engine.add_drawer(
        draw_noblock_input_obscure_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, GOLDEN_CUT_Y + 3]
        )
    )

    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, GOLDEN_CUT_Y],
            text="Please confirm the password for user " + f"{user_obj.name}"
        )
    )
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, GOLDEN_CUT_Y + 1],
            orientation="h",
            lengh=51,
            char="─"
        )
    )


def screan_password_missmatch(engine, LINE, SCREAN_CENTER_X, GOLDEN_CUT_Y, version):
    
    color_red = engine.color_conv.hex_to_color("f", 0, "ff0000")

    add_decorations(engine, version, LINE, SCREAN_CENTER_X)

    # BODY
    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, GOLDEN_CUT_Y],
            text="Password missmatch, try again",
            color_slot_1=color_red
        )
    )

    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, GOLDEN_CUT_Y + 1],
            orientation="h",
            lengh=51,
            char="─"
        )
    )

    engine.draw_frame_buffer()
    sleep(3)
