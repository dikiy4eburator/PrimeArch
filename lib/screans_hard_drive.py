import json
import os

from .e_ascii import (
    draw_text,
    draw_text_middle,
    draw_text_right,draw_line,
    draw_line_middle,
    draw_noblock_menu,
    draw_noblock_menu_middle,
    draw_noblock_menu_right,
    draw_noblock_input_obscure_middle,
    draw_noblock_work_indicator,
)

from .lib_objects import (
    add_decorations,
    btrfs_subvolume_list,
    menu_list_manual_mount,
    color_red,
    color_green,
    arch_blau
)

# Automatik funktions
# =======================================================================================
def screan_select_harddrive(engine, LINE, MENU_ORIGIN_Y, SCREAN_CENTER_X, MENU_OFFSET, sys_obj, version):

    hdd_name_list, hdd_size_list = sys_obj.hard_drive.get_hard_drives("hdd")
    add_decorations(engine, version, LINE, SCREAN_CENTER_X)

    # BODY
    engine.add_drawer(
        draw_noblock_menu_right(
            engine,
            hdd_name_list,
            menu_lengh=len(hdd_name_list),
            start_vektor=[SCREAN_CENTER_X - 1, LINE[0] + MENU_OFFSET + 6]
        )
    )
    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 3 + MENU_OFFSET],
            text="Select a hard drive"
        )
    )
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 4 + MENU_OFFSET],
            orientation="h",
            lengh=51,
            char="─"
        )
    )
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 7 + MENU_OFFSET + len(hdd_name_list)],
            orientation="h",
            lengh=41,
            char="─"
        )
    )

    # INFO
    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[1] - 13],
            text="!!! ATENTION !!!",
            color_slot_1=color_red
        )
    )
    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[1] - 12],
            text="the selected hard drive will be"
        )
    )
    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[1] - 11],
            text="WIPED",
            color_slot_1=color_red
        )
    )
    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[1] - 10],
            text="in the most BRUTAL and CUNNING way"
        )
    )

    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y + 2],
            text="[ESC] BACK TO SUMMARY"
        )
    )

    for i, _ in enumerate(hdd_size_list):
        engine.add_drawer(
            draw_text(
                engine,
                start_vektor=[SCREAN_CENTER_X + 1, LINE[0] + 6 + MENU_OFFSET + i],
                text=hdd_size_list[i]
            )
        )

# Manual mount funktions
# =======================================================================================
def screan_manual_partition(engine, MENU_ORIGIN_Y, LINE, SCREAN_CENTER_X, MENU_OFFSET, sys_obj, version):
    
    add_decorations(engine, version, LINE, SCREAN_CENTER_X)

    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 3 + MENU_OFFSET],
            text="Mount partitions"
        )
    )
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 4 + MENU_OFFSET],
            orientation="h",
            lengh=51,
            char="─"
        )
    )

    # BODY
    engine.add_drawer(
        draw_noblock_menu_right(
            engine,
            menu_list_manual_mount,
            menu_lengh=len(menu_list_manual_mount),
            start_vektor=[SCREAN_CENTER_X - 3, LINE[0] + 6 + MENU_OFFSET]
        )
    )

    for i, part in enumerate(sys_obj.partition_list):
        engine.add_drawer(
            draw_text(
                engine,
                start_vektor=[SCREAN_CENTER_X, LINE[0] + 6 + i + MENU_OFFSET],
                text=part
            )
        )

    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 11 + MENU_OFFSET],
            orientation="h",
            lengh=41,
            char="─"
        )
    )

    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 13 + MENU_OFFSET],
            text="[ESC] BACK TO SUMMARY"
        )
    )


def screan_manual_mount(engine, MENU_ORIGIN_Y, LINE, SCREAN_CENTER_X, MENU_OFFSET, sys_obj, version):

    hdd_name, hdd_lines, part_name, part_size = sys_obj.hard_drive.get_hard_drives("part")

    add_decorations(engine, version, LINE, SCREAN_CENTER_X)

    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 4 + MENU_OFFSET],
            orientation="h",
            lengh=51,
            char="─"
        )
    )

    # BODY
    engine.add_drawer(
        draw_noblock_menu(
            engine,
            part_name,
            menu_lengh=len(part_name),
            start_vektor=[SCREAN_CENTER_X - 5, LINE[0] + MENU_OFFSET + 6]
        )
    )

    pre_pos = 0
    for i in range(len(hdd_name)):
        engine.add_drawer(
            draw_text_right(
                engine,
                start_vektor=[SCREAN_CENTER_X - 10, LINE[0] + 6 + MENU_OFFSET + pre_pos + hdd_lines[i]],
                text=hdd_name[i]
            )
        )
        pre_pos = pre_pos + hdd_lines[i]

    for i in range(len(part_size)):
        engine.add_drawer(
            draw_text_right(
                engine,
                start_vektor=[SCREAN_CENTER_X + 15, LINE[0] + 6 + MENU_OFFSET + i],
                text=part_size[i]
            )
        )
