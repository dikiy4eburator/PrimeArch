from .e_ascii import (
    draw_text,
    draw_char,
    draw_text_middle,
    draw_text_right,draw_line,
    draw_line_middle,
    draw_noblock_menu,
    draw_noblock_menu_middle,
    draw_noblock_menu_right,
    draw_noblock_input_obscure_middle,
    draw_WI_matrix,
    draw_frame,
    Color,
)

from .lib_objects import (
    add_decorations,
    color_red,
    color_green,
    arch_blau
)

def menu_switcher(engine, sys_obj, SCREAN_CENTER_X, MENU_ORIGIN_Y, menu_number):
    """
    holds pre defined menus for use in screans
    """

    if menu_number == 1:

        # MENU 1
        engine.add_drawer(draw_text_middle(engine, start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y - 3], text="[ENTER] START"))
        engine.add_drawer(draw_text_middle(engine, start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y + 2], text="[ESC] EXIT"))

    if menu_number == 2:

        # MENU 2
        if sys_obj.installmode == "auto_ext4" or sys_obj.installmode == "auto_btrfs_suse_stile" or sys_obj.installmode == "auto_btrfs_minimal":
            if sys_obj.hddname is None:
                engine.add_drawer(
                    draw_text_middle(
                        engine,
                        start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y - 3],
                        text="[0] START INSTALL PROCESS",
                        color_slot_1=color_red
                    )
                )
            else:
                engine.add_drawer(
                    draw_text_middle(
                        engine,
                        start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y - 3],
                        text="[0] START INSTALL PROCESS",
                        color_slot_1=color_green))

        else:
            if sys_obj.partition_list[0] == "nothig mounted":
                engine.add_drawer(
                    draw_text_middle(
                        engine,
                        start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y - 3],
                        text="[0] START INSTALL PROCESS",
                        color_slot_1=color_red
                    )
                )
            else:
                engine.add_drawer(
                    draw_text_middle(
                        engine,
                        start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y - 3],
                        text="[0] START INSTALL PROCESS",
                        color_slot_1=color_green
                    )
                )

        engine.add_drawer(draw_text_middle(engine, start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y + 2], text="[ESC] EXIT"))

        engine.add_drawer(draw_text_right(engine, start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y - 1], text="  EDIT USER [1]"))
        engine.add_drawer(draw_text_right(engine, start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y], text=" TOGLE ROOT [2]"))
        engine.add_drawer(draw_text_middle(engine, start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y - 1], text="|"))
        engine.add_drawer(draw_text_middle(engine, start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y], text="|"))
        engine.add_drawer(draw_text(engine, start_vektor=[SCREAN_CENTER_X + 3, MENU_ORIGIN_Y - 1], text="[3] EDIT SYSTEM"))

        if sys_obj.installmode == "manual":
            engine.add_drawer(draw_text(engine, start_vektor=[SCREAN_CENTER_X + 3, MENU_ORIGIN_Y], text="[4] MOUNT PARTITIONS"))
        else:
            engine.add_drawer(draw_text(engine, start_vektor=[SCREAN_CENTER_X + 3, MENU_ORIGIN_Y], text="[4] SELECT HARDDRIVE"))

    if menu_number == 3:
        # MENU 3
        engine.add_drawer(draw_text_right(engine, start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y], text="YES [Y]"))
        engine.add_drawer(draw_text_middle(engine, start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y], text="|"))
        engine.add_drawer(draw_text(engine, start_vektor=[SCREAN_CENTER_X + 3, MENU_ORIGIN_Y], text="[N] NO"))

    if menu_number == 4:
        # MENU 4
        engine.add_drawer(draw_text_middle(engine, start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y - 3], text="[R] REBOOT"))
        engine.add_drawer(draw_text_middle(engine, start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y + 2], text="[S] EXIT"))


def screan_wellcom(engine, MENU_ORIGIN_Y, LINE, SCREAN_CENTER_X, sys_obj, version):

    add_decorations(engine, version, LINE, SCREAN_CENTER_X)

    # BODY
    with open("lib/resources/logo", "r") as logo:
        for i, x in enumerate(logo):
            engine.add_drawer(
                draw_text_middle(
                    engine,
                    start_vektor=[SCREAN_CENTER_X, int(LINE[0] + 5) + i],
                    text=f"{x}",
                    color_slot_1=arch_blau
                )
            )

    # MENU
    menu_switcher(engine, sys_obj, SCREAN_CENTER_X, MENU_ORIGIN_Y, 1)


def screan_summary(engine, MENU_ORIGIN_Y, LINE, SCREAN_CENTER_X, MENU_OFFSET, sys_obj, version):

    add_decorations(engine, version, LINE, SCREAN_CENTER_X)

    # BODY
    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 3 + MENU_OFFSET],
            text="SUMMARY"
        )
    )
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 4 + MENU_OFFSET],
            orientation="h",
            lengh=51,
            char="─"
        )
    )
    

    # Categori names
    engine.add_drawer(
        draw_text_right(
            engine,
            start_vektor=[SCREAN_CENTER_X - 18, LINE[0] + 6 + MENU_OFFSET],
            text="Users:"
        )
    )
    engine.add_drawer(
        draw_text_right(
            engine,
            start_vektor=[SCREAN_CENTER_X - 18, LINE[0] + 12 + MENU_OFFSET],
            text="System:"
        )
    )
    engine.add_drawer(
        draw_text_right(
            engine,
            start_vektor=[SCREAN_CENTER_X - 18, LINE[0] + 18 + MENU_OFFSET],
            text="Hard drive:"
        )
    )
    
    # # SYSTEM INFO
    # Info area LEFT
    engine.add_drawer(
        draw_text_right(
            engine,
            start_vektor=[SCREAN_CENTER_X + 1, LINE[0] + 6 + MENU_OFFSET],
            text="User name:"
        )
    )
    
    if sys_obj.users[1].password != "user":
        engine.add_drawer(
            draw_text_right(
                engine,
                start_vektor=[SCREAN_CENTER_X + 1, LINE[0] + 7 + MENU_OFFSET],
                text="Password:",
                color_slot_1=color_green
            )
        )
    else:
        engine.add_drawer(
            draw_text_right(
                engine,
                start_vektor=[SCREAN_CENTER_X + 1, LINE[0] + 7 + MENU_OFFSET],
                text="Password:",
                color_slot_1=color_red
            )
        )
    
    if sys_obj.users[0].status == "disabled":
        engine.add_drawer(
            draw_text_right(
                engine,
                start_vektor=[SCREAN_CENTER_X + 1, LINE[0] + 9 + MENU_OFFSET],
                text="root account:",
                color_slot_1=color_green
            )
        )
    else:
        engine.add_drawer(
            draw_text_right(
                engine,
                start_vektor=[SCREAN_CENTER_X + 1, LINE[0] + 9 + MENU_OFFSET],
                text="root account:",
                color_slot_1=color_red
            )
        )
        engine.add_drawer(
            draw_text_right(
                engine,
                start_vektor=[SCREAN_CENTER_X + 1, LINE[0] + 10 + MENU_OFFSET],
                text="root password:",
                color_slot_1=color_red
            )
        )
    
    engine.add_drawer(
        draw_text_right(
            engine,
            start_vektor=[SCREAN_CENTER_X + 1, LINE[0] + 12 + MENU_OFFSET],
            text="Hostname:"
        )
    )
    engine.add_drawer(
        draw_text_right(
            engine,
            start_vektor=[SCREAN_CENTER_X + 1, LINE[0] + 13 + MENU_OFFSET],
            text="Language:"
        )
    )
    engine.add_drawer(
        draw_text_right(
            engine,
            start_vektor=[SCREAN_CENTER_X + 1, LINE[0] + 14 + MENU_OFFSET],
            text="Time:"
        )
    )
    engine.add_drawer(
        draw_text_right(
            engine,
            start_vektor=[SCREAN_CENTER_X + 1, LINE[0] + 15 + MENU_OFFSET],
            text="Desktop:"
        )
    )
    engine.add_drawer(
        draw_text_right(
            engine,
            start_vektor=[SCREAN_CENTER_X + 1, LINE[0] + 16 + MENU_OFFSET],
            text="Grafikdriver:"
        )
    )

    if sys_obj.installmode != "manual":

        engine.add_drawer(
            draw_text_right(
                engine,
                start_vektor=[SCREAN_CENTER_X + 1, LINE[0] + 18 + MENU_OFFSET],
                text="Install mode:"
            )
        )
        
        if sys_obj.hddname is None:
            engine.add_drawer(
                draw_text_right(
                    engine,
                    start_vektor=[SCREAN_CENTER_X + 1, LINE[0] + 19 + MENU_OFFSET],
                    text="Hard drive:",
                    color_slot_1=color_red
                )
            )
        else:
            engine.add_drawer(
                draw_text_right(
                    engine,
                    start_vektor=[SCREAN_CENTER_X + 1, LINE[0] + 19 + MENU_OFFSET],
                    text="Hard drive:",
                    color_slot_1=color_green
                )
            )
        
        engine.add_drawer(
            draw_text_right(
                engine,
                start_vektor=[SCREAN_CENTER_X + 1, LINE[0] + 20 + MENU_OFFSET],
                text="SWAP size:"
            )
        )
        engine.add_drawer(
            draw_line(
                engine,
                start_vektor=[SCREAN_CENTER_X - 20, LINE[0] + 22 + MENU_OFFSET],
                orientation="h",
                lengh=41,
                char="─"
            )
        )
    else:
        engine.add_drawer(
            draw_text_right(
                engine,
                start_vektor=[SCREAN_CENTER_X + 1, LINE[0] + 18 + MENU_OFFSET],
                text="Install mode:"
            )
        )
        engine.add_drawer(
            draw_text_right(
                engine,
                start_vektor=[SCREAN_CENTER_X + 1, LINE[0] + 19 + MENU_OFFSET],
                text="ROOT:"
            )
        )
        engine.add_drawer(
            draw_text_right(
                engine,
                start_vektor=[SCREAN_CENTER_X + 1, LINE[0] + 20 + MENU_OFFSET],
                text="BOOT:"
            )
        )
        engine.add_drawer(
            draw_text_right(
                engine,
                start_vektor=[SCREAN_CENTER_X + 1, LINE[0] + 21 + MENU_OFFSET],
                text="HOME:"
            )
        )
        engine.add_drawer(
            draw_text_right(
                engine,
                start_vektor=[SCREAN_CENTER_X + 1, LINE[0] + 22 + MENU_OFFSET],
                text="SWAP:"
            )
        )
        engine.add_drawer(
            draw_line_middle(
                engine,
                start_vektor=[SCREAN_CENTER_X + 1, LINE[0] + 25 + MENU_OFFSET],
                orientation="h",
                lengh=41,
                char="─"
            )
        )
    

    # Info area RIGHT
    engine.add_drawer(
        draw_text(
            engine,
            start_vektor=[SCREAN_CENTER_X + 2, LINE[0] + 6 + MENU_OFFSET],
            text=f"{sys_obj.users[1].name}"
        )
    )
    if sys_obj.users[1].password != "user":
        engine.add_drawer(
            draw_text(
                engine,
                start_vektor=[SCREAN_CENTER_X + 2, LINE[0] + 7 + MENU_OFFSET],
                text=f"{'*' * len(sys_obj.users[1].password)}",
                color_slot_1=color_green
            )
        )
    else:
        engine.add_drawer(
            draw_text(
                engine,
                start_vektor=[SCREAN_CENTER_X + 2, LINE[0] + 7 + MENU_OFFSET],
                text=f"{sys_obj.users[1].password}",
                color_slot_1=color_red
            )
        )

    if sys_obj.users[0].status == "disabled":
        engine.add_drawer(
            draw_text(
                engine,
                start_vektor=[SCREAN_CENTER_X + 2, LINE[0] + 9 + MENU_OFFSET],
                text=f"{sys_obj.users[0].status}",
                color_slot_1=color_green
            )
        )
    else:
        engine.add_drawer(
            draw_text(
                engine,
                start_vektor=[SCREAN_CENTER_X + 2, LINE[0] + 9 + MENU_OFFSET],
                text=f"{sys_obj.users[0].status}",
                color_slot_1=color_red
            )
        )
        engine.add_drawer(
            draw_text(
                engine,
                start_vektor=[SCREAN_CENTER_X + 2, LINE[0] + 10 + MENU_OFFSET],
                text=f"{'*' * len(sys_obj.users[0].password)}"
            )
        )
    
    engine.add_drawer(
        draw_text(
            engine,
            start_vektor=[SCREAN_CENTER_X + 2, LINE[0] + 12 + MENU_OFFSET],
            text=f"{sys_obj.hostname}"
        )
    )
    engine.add_drawer(
        draw_text(
            engine,
            start_vektor=[SCREAN_CENTER_X + 2, LINE[0] + 13 + MENU_OFFSET],
            text=f"{sys_obj.language}"
        )
    )
    engine.add_drawer(
        draw_text(
            engine,
            start_vektor=[SCREAN_CENTER_X + 2, LINE[0] + 14 + MENU_OFFSET],
            text=f"{sys_obj.sys_time[0]} / {sys_obj.sys_time[1]} / {sys_obj.sys_time[2]}"
        )
    )
    engine.add_drawer(
        draw_text(
            engine,
            start_vektor=[SCREAN_CENTER_X + 2, LINE[0] + 15 + MENU_OFFSET],
            text=f"{sys_obj.desktop}"
        )
    )
    engine.add_drawer(
        draw_text(
            engine,
            start_vektor=[SCREAN_CENTER_X + 2, LINE[0] + 16 + MENU_OFFSET],
            text=f"{sys_obj.grafikdriver}"
        )
    )
   
    # # HARD DRIVE
    # Auto
    if sys_obj.installmode != "manual":

        engine.add_drawer(
            draw_text(
                engine,
                start_vektor=[SCREAN_CENTER_X + 2, LINE[0] + 18 + MENU_OFFSET],
                text=f"{sys_obj.installmode}"
            )
        )
        
        if sys_obj.hddname is None:
            engine.add_drawer(
                draw_text(
                    engine,
                    start_vektor=[SCREAN_CENTER_X + 2, LINE[0] + 19 + MENU_OFFSET],
                    text=f"{sys_obj.hddname}",
                    color_slot_1=color_red
                )
            )
        else:
            engine.add_drawer(
                draw_text(
                    engine,
                    start_vektor=[SCREAN_CENTER_X + 2, LINE[0] + 19 + MENU_OFFSET],
                    text=f"{sys_obj.hddname}",
                    color_slot_1=color_green
                )
            )
        
        engine.add_drawer(
            draw_text(
                engine,
                start_vektor=[SCREAN_CENTER_X + 2, LINE[0] + 20 + MENU_OFFSET],
                text=f"{sys_obj.swapsize} Mb"
            )
        )
   
    # manual
    else:
        engine.add_drawer(
            draw_text(
                engine,
                start_vektor=[SCREAN_CENTER_X + 2, LINE[0] + 18 + MENU_OFFSET],
                text=f"{sys_obj.installmode}"
            )
        )
        for i, part in enumerate(sys_obj.partition_list):
            engine.add_drawer(
                draw_text(
                    engine,
                    start_vektor=[SCREAN_CENTER_X + 2, LINE[0] + 19 + i + MENU_OFFSET],
                    text=part
                )
            )


    # MENU
    menu_switcher(engine, sys_obj, SCREAN_CENTER_X, MENU_ORIGIN_Y, 2)


def screan_abort(engine, MENU_ORIGIN_Y, LINE, SCREAN_CENTER_X, sys_obj, version):

    add_decorations(engine, version, LINE, SCREAN_CENTER_X)

    #BODY
    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 6],
            text="EXIT SCRIPT ?"
        )
    )
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 7],
            orientation="h",
            lengh=51,
            char="─"
        )
    )

    # MENU
    menu_switcher(engine, sys_obj, SCREAN_CENTER_X, MENU_ORIGIN_Y, 3)


def screan_end(engine, MENU_ORIGIN_Y, LINE, SCREAN_CENTER_X, sys_obj, version):

    add_decorations(engine, version, LINE, SCREAN_CENTER_X)

    # BODY
    with open("lib/resources/logo", "r") as logo:
        for i, x in enumerate(logo):
            engine.add_drawer(
                draw_text_middle(
                    engine,
                    start_vektor=[SCREAN_CENTER_X, int(LINE[0] + 5) + i],
                    text=f"{x}",
                    color_slot_1=arch_blau
                )
            )


    # MENU
    menu_switcher(engine, sys_obj, SCREAN_CENTER_X, MENU_ORIGIN_Y, 4)


def screan_install_info(engine, text, LINE, SCREAN_CENTER_X, golden_cut_y, version):

    add_decorations(engine, version, LINE, SCREAN_CENTER_X)

    # BODY
    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, golden_cut_y],
            text="Installation in progress"
        )
    )
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, golden_cut_y + 1],
            orientation="h",
            lengh=51,
            char="─"
        )
    )



    # draw workindikator
    engine.add_drawer(
        draw_WI_matrix(
            engine,
            [SCREAN_CENTER_X - 2, LINE[1] - 10],
            [5, 5]
        )
    )
    # draw lines
    engine.add_drawer(
        draw_line(
            engine,
            start_vektor=[SCREAN_CENTER_X - 13, LINE[1] - 10],
            orientation="h",
            lengh=11,
            char="─"
        )
    )
    engine.add_drawer(
        draw_line(
            engine,
            start_vektor=[SCREAN_CENTER_X + 3, LINE[1] - 10],
            orientation="h",
            lengh=11,
            char="─"
        )
    )
    engine.add_drawer(
        draw_line(
            engine,
            start_vektor=[SCREAN_CENTER_X - 3, LINE[1] - 5],
            orientation="h",
            lengh=7,
            char="─"
        )
    )
    engine.add_drawer(
        draw_line(
            engine,
            start_vektor=[SCREAN_CENTER_X - 3, LINE[1] - 10],
            orientation="v",
            lengh=6,
            char="│"
        )
    )
    engine.add_drawer(
        draw_line(
            engine,
            start_vektor=[SCREAN_CENTER_X + 3, LINE[1] - 10],
            orientation="v",
            lengh=6,
            char="│"
        )
    )
    # draw corners
    engine.add_drawer(
        draw_char(
            engine,
            start_vektor=[SCREAN_CENTER_X - 3, LINE[1] - 10],
            char="┐"
        )
    )
    engine.add_drawer(
        draw_char(
            engine,
            start_vektor=[SCREAN_CENTER_X + 3, LINE[1] - 10],
            char="┌"
        )
    )
    engine.add_drawer(
        draw_char(
            engine,
            start_vektor=[SCREAN_CENTER_X - 3, LINE[1] - 5],
            char="└"
        )
    )
    engine.add_drawer(
        draw_char(
            engine,
            start_vektor=[SCREAN_CENTER_X + 3, LINE[1] - 5],
            char="┘"
        )
    )
    


    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, golden_cut_y + 3],
            text=text
        )
    )

    engine.redraw = True
