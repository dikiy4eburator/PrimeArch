# e_ascii  Copyright (C) 2020  Wladimir Frank

from .helper import Color, Tile

# #####################################################################
# ###     DRAWER     ##################################################
# #####################################################################


class draw_char:
    """
    posible parameters are:
        - char          <---- chr(),    to be printed
        - start_vektor  <---- list(),   [x, y]
        - color_slot_1  <---- str(),    forgrund collor in form of shell string
        - color_slot_2  <---- str(),    backgrund collor in form of shell string
    """

    def __init__(self, engine, **parameter):
        
        self.engine = engine
        self.kind = "drawer"

        self.start_x = 1
        self.start_y = 1

        self.char = " "
        self.color_slot_1 = Color.ENDC
        self.color_slot_2 = Color.ENDC


        for entry in parameter:
            if "char" in entry:
                self.char = parameter['char']

            elif "start_vektor" in entry:
                self.start_x = parameter['start_vektor'][0]
                self.start_y = parameter['start_vektor'][1]

            elif "color_slot_1" in entry:
                 self.color_slot_1 = parameter['color_slot_1']

            elif "color_slot_2" in entry:
                 self.color_slot_2 = parameter['color_slot_2']

            else:
                pass

        self.draw()
    
    def change_parameter(self,  **parameter):
        for entry in parameter:
            if "char" in entry:
                self.char = parameter['char']

            elif "start_vektor" in entry:
                self.start_x = parameter['start_vektor'][0]
                self.start_y = parameter['start_vektor'][1]

            elif "color_slot_1" in entry:
                 self.color_slot_1 = parameter['color_slot_1']

            elif "color_slot_2" in entry:
                 self.color_slot_2 = parameter['color_slot_2']

            else:
                pass

        self.draw()
    
    def draw(self):
        # DO NOT draw outside FB
        if (
            self.start_y > 0 and
            self.start_y < self.engine.y_max and
            self.start_x > 0 and
            self.start_x < self.engine.x_max
        ):

            self.engine.FB[self.start_y][self.start_x] = Tile(
                self.char,
                self.color_slot_1,
                self.color_slot_2
            )

# ---------------------

class draw_text:
    """
    posible parameters are:
        - text           <---- str() ,  text to be printed
        - start_vektor   <---- list(),  [x, y]
        - color_slot_1   <---- str(),   forgrund collor in form of shell string
        - color_slot_2   <---- str(),   backgrund collor in form of shell string
    """

    def __init__(self, engine, **parameter):

        self.engine = engine
        self.kind = "drawer"

        self.start_x = 1
        self.start_y = 1

        self.text = " "
        self.color_slot_1 = Color.ENDC
        self.color_slot_2 = Color.ENDC

        for entry in parameter:
            if "text" in entry:
                self.text = parameter['text']

            elif "start_vektor" in entry:
                self.start_x = parameter['start_vektor'][0]
                self.start_y = parameter['start_vektor'][1]

            elif "color_slot_1" in entry:
                 self.color_slot_1 = parameter['color_slot_1']

            elif "color_slot_2" in entry:
                 self.color_slot_2 = parameter['color_slot_2']

            else:
                pass

        self.draw()

    def change_parameter(self,  **parameter):
        for entry in parameter:
            if "text" in entry:
                self.text = parameter['text']

            elif "start_vektor" in entry:
                self.start_x = parameter['start_vektor'][0]
                self.start_y = parameter['start_vektor'][1]

            elif "color_slot_1" in entry:
                 self.color_slot_1 = parameter['color_slot_1']

            elif "color_slot_2" in entry:
                 self.color_slot_2 = parameter['color_slot_2']

            else:
                pass

        self.draw()

    def draw(self):
        for i in range(len(self.text)):
            # DO NOT draw outside FB
            if (
                self.start_y > 0 and
                self.start_y < self.engine.y_max and
                self.start_x + i > 0 and
                self.start_x + i < self.engine.x_max
            ):

                self.engine.FB[self.start_y][self.start_x + i] = Tile(
                    self.text[i],
                    self.color_slot_1,
                    self.color_slot_2
                )


class draw_text_middle(draw_text):

    def draw(self):

        for i in range(len(self.text)):
            # DO NOT draw outside FB
            if (
                self.start_y > 0 and
                self.start_y < self.engine.y_max and
                self.start_x - int(len(self.text) / 2) + i > 0 and
                self.start_x - int(len(self.text) / 2) + i < self.engine.x_max
            ):

                self.engine.FB[self.start_y][self.start_x - int(len(self.text) / 2) + i + 1] = Tile(
                    self.text[i],
                    self.color_slot_1,
                    self.color_slot_2
                )


class draw_text_right(draw_text):

    def draw(self):

        for i in range(len(self.text)):
            # DO NOT draw outside FB
            if (
                self.start_y > 0 and
                self.start_y < self.engine.y_max and
                self.start_x - len(self.text) + i > 0 and
                self.start_x - len(self.text) + i < self.engine.x_max
            ):

                self.engine.FB[self.start_y][self.start_x - len(self.text) + i] = Tile(
                    self.text[i],
                    self.color_slot_1,
                    self.color_slot_2
                )

# ---------------------

class draw_line:
    """
    posible parameters are:
        !!! keep in mind, that the lines are printed
        from left to right and from top to bottom !!!

        - char              <---- chr(),   character to be used to draw the line
        - start_vektor      <---- list(),  [x, y]
        - orientation       <---- str(),   either "v" -vertikal or "h" -horizontal
        - lengh             <---- int(),   lengh of the line
        - color_slot_1      <---- str(),   forgrund collor in form of shell string
        - color_slot_2      <---- str(),   backgrund collor in form of shell string
    """

    def __init__(self, engine, **parameter):

        self.engin_obj = engine
        self.kind = "drawer"

        self.start_x = 1
        self.start_y = 1

        self.orientation = ""
        self.lengh = 3
        self.char = "─"
        self.color_slot_1 = Color.ENDC
        self.color_slot_2 = Color.ENDC

        for entry in parameter:
            if "char" in entry:
                self.char = parameter['char']

            elif "orientation" in entry:
                 self.orientation = parameter['orientation']
            
            elif "lengh" in entry:
                 self.lengh = parameter['lengh']

            elif "start_vektor" in entry:
                self.start_x = parameter['start_vektor'][0]
                self.start_y = parameter['start_vektor'][1]

            elif "color_slot_1" in entry:
                 self.color_slot_1 = parameter['color_slot_1']

            elif "color_slot_2" in entry:
                 self.color_slot_2 = parameter['color_slot_2']

            else:
                pass

        self.draw()

    def change_parameter(self,  **parameter):
        for entry in parameter:
            if "char" in entry:
                self.char = parameter['char']

            elif "orientation" in entry:
                 self.orientation = parameter['orientation']
            
            elif "lengh" in entry:
                 self.lengh = parameter['lengh']

            elif "start_vektor" in entry:
                self.start_x = parameter['start_vektor'][0]
                self.start_y = parameter['start_vektor'][1]

            elif "color_slot_1" in entry:
                 self.color_slot_1 = parameter['color_slot_1']

            elif "color_slot_2" in entry:
                 self.color_slot_2 = parameter['color_slot_2']

            else:
                pass

        self.draw()

    def draw(self):

        if self.orientation == "h":
            for i in range(self.lengh):
                # DO NOT draw outside FB
                if (
                    self.start_y > 0 and
                    self.start_y < self.engin_obj.y_max and
                    self.start_x + i > 0 and
                    self.start_x + i < self.engin_obj.x_max
                ):

                    self.engin_obj.FB[self.start_y][self.start_x + i] = Tile(
                        self.char,
                        self.color_slot_1,
                        self.color_slot_2
                    )

        if self.orientation == "v":
            for i in range(self.lengh):
                # DO NOT draw outside FB
                if (
                    self.start_y + i > 0 and
                    self.start_y + i < self.engin_obj.y_max and
                    self.start_x > 0 and
                    self.start_x < self.engin_obj.x_max
                ):

                    self.engin_obj.FB[i + self.start_y][self.start_x] = Tile(
                        self.char,
                        self.color_slot_1,
                        self.color_slot_2
                    )


class draw_line_middle(draw_line):

    def __init__(self, engine, **parameter):
        self.engin_obj = engine
        self.kind = "drawer"

        self.start_x = 1
        self.start_y = 1

        self.orientation = ""
        self.lengh = 3
        self.char = "─"
        self.color_slot_1 = Color.ENDC
        self.color_slot_2 = Color.ENDC

        for entry in parameter:
            if "char" in entry:
                self.char = parameter['char']

            elif "orientation" in entry:
                 self.orientation = parameter['orientation']
            
            elif "lengh" in entry:
                 self.lengh = parameter['lengh']

            elif "start_vektor" in entry:
                self.start_x = parameter['start_vektor'][0]
                self.start_y = parameter['start_vektor'][1]

            elif "color_slot_1" in entry:
                 self.color_slot_1 = parameter['color_slot_1']

            elif "color_slot_2" in entry:
                 self.color_slot_2 = parameter['color_slot_2']

            else:
                pass
        
        if self.orientation == "h":
            self.start_x -= int(self.lengh / 2)
        elif self.orientation == "v":
            self.start_y = - int(self.lengh / 2)

        self.draw()

    def change_parameter(self,  **parameter):
        for entry in parameter:
            if "char" in entry:
                self.char = parameter['char']

            elif "orientation" in entry:
                 self.orientation = parameter['orientation']
            
            elif "lengh" in entry:
                 self.lengh = parameter['lengh']

            elif "start_vektor" in entry:
                self.start_x = parameter['start_vektor'][0]
                self.start_y = parameter['start_vektor'][1]

            elif "color_slot_1" in entry:
                 self.color_slot_1 = parameter['color_slot_1']

            elif "color_slot_2" in entry:
                 self.color_slot_2 = parameter['color_slot_2']

            else:
                pass
        
        if self.orientation == "h":
            self.start_x -= int(self.lengh / 2)
        elif self.orientation == "v":
            self.start_y = - int(self.lengh / 2)

        self.draw()

# ---------------------

class draw_frame:
    """
    posible parameters are:
        - name          <---- str(), name of the frame
        - start_vektor  <---- list(), [x, y]
        - end_vektor    <---- list(), [x, y]
    """
    
    def __init__(self, engine, **parameter):
        self.engine = engine
        self.name = None
        self.start_vektor = [1, 1]
        self.end_vektor = [3, 3]

        for entry in parameter:
            if "name" in entry:
                self.name = parameter['name']
            elif "start_vektor" in entry:
                self.start_vektor = parameter['start_vektor']
            elif "end_vektor" in entry:
                self.end_vektor = parameter['end_vektor']
            else:
                pass

        self.lengh_x = self.end_vektor[0] - self.start_vektor[0]
        self.lengh_y = self.end_vektor[1] - self.start_vektor[1]

        self.draw()
    
    def change_parameter(self,  **parameter):
        for entry in parameter:
            if "name" in entry:
                self.name = parameter['name']
            elif "start_vektor" in entry:
                self.start_vektor = parameter['start_vektor']
            elif "end_vektor" in entry:
                self.end_vektor = parameter['end_vektor']
            else:
                pass

        self.lengh_x = self.end_vektor[0] - self.start_vektor[0]
        self.lengh_y = self.end_vektor[1] - self.start_vektor[1]

        self.draw()

    def draw(self):

        # Draw Border
        self.engine.add_drawer(
            draw_line(
                self.engine,
                start_vektor=[self.start_vektor[0], self.start_vektor[1]],
                orientation="h",
                lengh=self.lengh_x + 1,
                char="─"
            )
        )
        self.engine.add_drawer(
            draw_line(
                self.engine,
                start_vektor=[self.start_vektor[1], self.start_vektor[1] + self.lengh_y],
                orientation="h",
                lengh=self.lengh_x + 1,
                char="─"
            )
        )
        self.engine.add_drawer(
            draw_line(
                self.engine,
                start_vektor=[self.start_vektor[0], self.start_vektor[1]],
                orientation="v",
                lengh=self.lengh_y + 1,
                char="│"
            )
        )
        self.engine.add_drawer(
            draw_line(
                self.engine,
                start_vektor=[self.start_vektor[0] + self.lengh_x, self.start_vektor[1]],
                orientation="v",
                lengh=self.lengh_y + 1,
                char="│"
            )
        )

        # Draw corners
        self.engine.add_drawer(
            draw_char(
                self.engine,
                start_vektor=[self.start_vektor[0], self.start_vektor[1]],
                char="┌"
            )
        )
        self.engine.add_drawer(
            draw_char(
                self.engine,
                start_vektor=[self.start_vektor[0] + self.lengh_x, self.start_vektor[1]],
                char="┐"
            )
        )
        self.engine.add_drawer(
            draw_char(
                self.engine,
                start_vektor=[self.start_vektor[0], self.start_vektor[1] + self.lengh_y],
                char="└"
            )
        )
        self.engine.add_drawer(
            draw_char(
                self.engine,
                start_vektor=[self.start_vektor[0] + self.lengh_x, self.start_vektor[1] + self.lengh_y],
                char="┘"
            )
        )

        # Draw name
        if self.name is not None:
            self.engine.add_drawer(
                draw_text(
                    self.engine,
                    start_vektor = [self.start_vektor[0] + 2, self.start_vektor[1]],
                    text = f"[ {self.name} ]"
                )
            )
