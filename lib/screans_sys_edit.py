from .e_ascii import (
    draw_text_middle,
    draw_line,
    draw_line_middle,
    draw_noblock_menu,
    draw_noblock_menu_middle,
    draw_noblock_input_middle
)

from .lib_objects import (
    add_decorations
)

def screan_edit_sys_params(engine, LINE, MENU_ORIGIN_Y, SCREAN_CENTER_X, MENU_OFFSET, sys_edit_menu, version):

    add_decorations(engine, version, LINE, SCREAN_CENTER_X)

    # BODY
    engine.add_drawer(
        draw_noblock_menu_middle(
            engine,
            sys_edit_menu,
            menu_lengh=len(sys_edit_menu),
            start_vektor=[SCREAN_CENTER_X, LINE[0] + MENU_OFFSET + 6]
        )
    )

    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 3 + MENU_OFFSET],
            text="Edit System Parameters"))
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 4 + MENU_OFFSET],
            orientation="h",
            lengh=51,
            char="─"))
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 7 + MENU_OFFSET + len(sys_edit_menu)],
            orientation="h",
            lengh=41,
            char="─"))

    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y + 2],
            text="[ESC] BACK TO SUMMARY"))


def screan_set_instal_mod(engine, LINE, MENU_ORIGIN_Y, SCREAN_CENTER_X, MENU_OFFSET, mod_list, version):

    add_decorations(engine, version, LINE, SCREAN_CENTER_X)

    # BODY
    engine.add_drawer(
        draw_noblock_menu_middle(
            engine,
            mod_list,
            menu_lengh=len(mod_list),
            start_vektor=[SCREAN_CENTER_X, LINE[0] + MENU_OFFSET + 6]
        )
    )

    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 3 + MENU_OFFSET],
            text="Selekt instal mod"
        )
    )
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 4 + MENU_OFFSET],
            orientation="h",
            lengh=51,
            char="─"
        )
    )
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 7 + MENU_OFFSET + len(mod_list)],
            orientation="h",
            lengh=41,
            char="─"
        )
    )

    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y + 2],
            text="[ESC] BACK TO SUMMARY"
        )
    )


def screan_set_languge(engine, LINE, MENU_ORIGIN_Y, SCREAN_CENTER_X, MENU_OFFSET, lang_list, version):

    add_decorations(engine, version, LINE, SCREAN_CENTER_X)

    # BODY
    engine.add_drawer(
        draw_noblock_menu_middle(
            engine,
            lang_list,
            menu_lengh=len(lang_list),
            start_vektor=[SCREAN_CENTER_X, LINE[0] + MENU_OFFSET + 6]
        )
    )

    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 3 + MENU_OFFSET],
            text="Select system languge"
        )
    )
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 4 + MENU_OFFSET], orientation="h", lengh=51, char="─"))
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 7 + MENU_OFFSET + len(lang_list)], orientation="h", lengh=41, char="─"))

    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y + 2],
            text="[ESC] BACK TO SUMMARY"
        )
    )


def screan_select_desktop(engine, LINE, MENU_ORIGIN_Y, SCREAN_CENTER_X, MENU_OFFSET, desktop_list, version):

    add_decorations(engine, version, LINE, SCREAN_CENTER_X)

    # BODY
    engine.add_drawer(
        draw_noblock_menu_middle(
            engine,
            desktop_list,
            menu_lengh=len(desktop_list),
            start_vektor=[SCREAN_CENTER_X, LINE[0] + MENU_OFFSET + 6]
        )
    )

    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 3 + MENU_OFFSET],
            text="select desktop"
        )
    )
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 4 + MENU_OFFSET],
            orientation="h",
            lengh=51,
            char="─"
        )
    )
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 7 + MENU_OFFSET + len(desktop_list)],
            orientation="h",
            lengh=41,
            char="─"
        )
    )

    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y + 2],
            text="[ESC] BACK TO SUMMARY"
        )
    )


def screan_select_graphic_card(engine, LINE, MENU_ORIGIN_Y, SCREAN_CENTER_X, MENU_OFFSET, grafic_list, version):

    add_decorations(engine, version, LINE, SCREAN_CENTER_X)

    # BODY
    engine.add_drawer(
        draw_noblock_menu_middle(
            engine,
            grafic_list,
            menu_lengh=len(grafic_list),
            start_vektor=[SCREAN_CENTER_X, LINE[0] + MENU_OFFSET + 6]
        )
    )

    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 3 + MENU_OFFSET],
            text="Select graphic card"
        )
    )
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 4 + MENU_OFFSET],
            orientation="h",
            lengh=51,
            char="─"
        )
    )
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 7 + MENU_OFFSET + len(grafic_list)],
            orientation="h",
            lengh=41,
            char="─"
        )
    )

    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y + 2],
            text="[ESC] BACK TO SUMMARY"
        )
    )


def screan_set_time_zone(engine, LINE, MENU_ORIGIN_Y, SCREAN_CENTER_X, MENU_OFFSET, zone_menu_list, version):

    add_decorations(engine, version, LINE, SCREAN_CENTER_X)

    # BODY
    engine.add_drawer(
        draw_noblock_menu_middle(
            engine,
            zone_menu_list,
            menu_lengh=len(zone_menu_list),
            start_vektor=[SCREAN_CENTER_X, LINE[0] + MENU_OFFSET + 6]
        )
    )

    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 3 + MENU_OFFSET],
            text="Set time zone"
        )
    )
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 4 + MENU_OFFSET],
            orientation="h",
            lengh=51,
            char="─"
        )
    )
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 7 + MENU_OFFSET + len(zone_menu_list)],
            orientation="h",
            lengh=41,
            char="─"
        )
    )

    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y + 2],
            text="[ESC] BACK TO SUMMARY"
        )
    )


def screan_set_time_subzone(engine, LINE, MENU_ORIGIN_Y, SCREAN_CENTER_X, MENU_OFFSET, sub_zone_menu_list, version):

    add_decorations(engine, version, LINE, SCREAN_CENTER_X)

    # BODY
    engine.add_drawer(
        draw_noblock_menu_middle(
            engine,
            sub_zone_menu_list,
            menu_lengh=10,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + MENU_OFFSET + 6]
        )
    )

    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 3 + MENU_OFFSET],
            text="Set time subzone"
        )
    )
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 4 + MENU_OFFSET],
            orientation="h",
            lengh=51,
            char="─"
        )
    )
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 7 + MENU_OFFSET + 10],
            orientation="h",
            lengh=41,
            char="─"
        )
    )

    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y + 2],
            text="[ESC] BACK TO SUMMARY"
        )
    )


def screan_set_time_format(engine, LINE, MENU_ORIGIN_Y, SCREAN_CENTER_X, MENU_OFFSET, time_format, version):

    add_decorations(engine, version, LINE, SCREAN_CENTER_X)

    # BODY
    engine.add_drawer(
        draw_noblock_menu_middle(
            engine,
            time_format,
            menu_lengh=len(time_format),
            start_vektor=[SCREAN_CENTER_X, LINE[0] + MENU_OFFSET + 6]
        )
    )
    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 3 + MENU_OFFSET],
            text="Set time format"
        )
    )
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 4 + MENU_OFFSET],
            orientation="h",
            lengh=51,
            char="─"
        )
    )
    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[0] + 7 + MENU_OFFSET + len(time_format)],
            orientation="h",
            lengh=41,
            char="─"
        )
    )
    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, MENU_ORIGIN_Y + 2],
            text="[ESC] BACK TO SUMMARY"
        )
    )


def screan_set_hostname(engine, LINE, SCREAN_CENTER_X, GOLDEN_CUT_Y, version):

    add_decorations(engine, version, LINE, SCREAN_CENTER_X)

    # BODY
    engine.add_drawer(
        draw_noblock_input_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, GOLDEN_CUT_Y + 3]
        )
    )

    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, GOLDEN_CUT_Y],
            text="Set hostname"
        )
    )

    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, GOLDEN_CUT_Y + 1],
            orientation="h",
            lengh=51,
            char="─"
        )
    )


def screan_set_swap(engine, LINE, SCREAN_CENTER_X, GOLDEN_CUT_Y, version):

    add_decorations(engine, version, LINE, SCREAN_CENTER_X)

    # BODY
    engine.add_drawer(
        draw_noblock_input_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, GOLDEN_CUT_Y + 3]
        )
    )

    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, GOLDEN_CUT_Y],
            text="Set SWAP size"
        )
    )

    engine.add_drawer(
        draw_line_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, GOLDEN_CUT_Y + 1],
            orientation="h", lengh=51, char="─"
        )
    )
