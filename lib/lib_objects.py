import json
import os
from datetime import datetime
from time import sleep

from .e_ascii import (
    draw_text_middle,
    draw_line,
    Color
)

from subprocess import (
    call,
    Popen,
    check_output,
    run,
    DEVNULL,
    PIPE
)

# Color Defs
color_converter = Color()

color_red = color_converter.rgb_to_color("f", 0, 220, 0, 0)
color_green = color_converter.rgb_to_color("f", 0, 0, 220, 0)
arch_blau = color_converter.rgb_to_color("f", 0, int(0.076 * 255), int(0.456 * 255), int(0.956 * 255))
color_gray = color_converter.rgb_to_color("f", 0, 64, 64, 64)

# ===============================

# Menu lists
menu_list_language = [
    "german",
    "english",
    "russian"
]

menu_list_desktop = [
    "gnome",
    "cinnamon",
    "xfce4",
    "mate",
    "no_desktop"
]

menu_list_grafikdriver = [
    "virtualbox",
    "nvidia",
    "nouveau",
    "ati",
    "intel",
    "no_grafik"
]

menu_list_installmode = [
    "auto_ext4",
    "auto_btrfs_suse_stile",
    "auto_btrfs_minimal",
    "manual"
]

menu_list_time_format = [
    "utc",
    "localtime"
]

menu_list_sys_edit = [
    "Install Mode",
    "Language",
    "Time",
    "Desktop",
    "Grafic Driver",
    "Hostname",
    "SWAP size"
]

menu_list_manual_mount = [
    "Mount ROOT",
    "Mount BOOT",
    "Mount HOME",
    "Mount SWAP",
]

btrfs_subvolume_list = [
    ".snapshots",
    "boot/grub2/i386-pc",
    "boot/grub2/x86_64-efi",
    "home",
    "opt",
    "root",
    "srv",
    "tmp",
    "usr/local",
    "var"
]

btrfs_subvolume_list_minimal = [
    "@",
    "@home",
    "@pkg",
    "@log"
]

daemons = [
    # general daemons
    "systemctl enable acpid",
    "systemctl enable ntpd",
    "systemctl enable cronie",
    
    # network managment
    "systemctl enable avahi-daemon",
    "systemctl enable wpa_supplicant",
    "systemctl enable NetworkManager",
    "systemctl enable sshd.socket",
    
    # NFS utils
    "systemctl enable rpcbind",
    
    # Snapd
    "systemctl enable snapd.socket"
]

# ================================

def add_decorations(engine, version, LINE, SCREAN_CENTER_X):
    engine.draw_list.clear()
    engine.clear_frame_buffer()

    # HEAD
    engine.add_drawer(
        draw_line(
            engine,
            start_vektor=[0, LINE[0]],
            orientation="h",
            lengh=engine.x_max,
            char="─"
        )
    )
    engine.add_drawer(
        draw_line(
            engine,
            start_vektor=[0, LINE[1]],
            orientation="h",
            lengh=engine.x_max,
            char="─"
        )
    )

    # COPY RIGHT MARK
    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[1] + 1],
            text="PrimeArch Copyright (C) 2020 Wladimir Frank",
            color_slot_1=color_gray
        )
    )

    # DEV MARK
    engine.add_drawer(
        draw_text_middle(
            engine,
            start_vektor=[SCREAN_CENTER_X, LINE[1] + 2],
            text=f"{version[0]} ver. {version[1]}",
            color_slot_1=color_gray
        )
    )

# ================================


class Target_User:
    """
    class for user info.
    kontains the name pasword and the status of the account.
    """
    def __init__(self, name, password, status):
        self.name = name
        self.password = password
        self.status = status


class Hard_drive:
    """
    class to work with hard drives
    """
    def __init__(self, parent):
        self.parent = parent
        self.hard_drive_list = json.loads(check_output(["lsblk", "-J"]))
    
    def get_hard_drives(self, what_return):
        
        hdd_name = list()
        hdd_size = list()
        part_name = list()
        part_size = list()

        hdd_lines = list()
        hdd_lines.append(0)
        
        for blockdevice in self.hard_drive_list['blockdevices']:
            hdd_name.append(f"{blockdevice['name']}")
            hdd_size.append(f"{blockdevice['size']}")
            if 'children' in blockdevice:
                hdd_lines.append(len(blockdevice['children']) + 1)
                for child in blockdevice['children']:
                    part_name.append(child['name'])
                    part_size.append(child['size'])
                part_name.append("")
                part_size.append("")
        
        hdd_lines = hdd_lines[:-1]

        if what_return == "hdd":
            return hdd_name, hdd_size
        if what_return == "part":
            return hdd_name, hdd_lines, part_name, part_size
            
    def auto_wipe_and_create(self):

        # WIPE DRIVE
        self.parent.run_command(f"wipefs -a -f /dev/{self.parent.hddname}")
        self.parent.run_command(f"sgdisk -Z /dev/{self.parent.hddname}")
        self.parent.run_command(f"sgdisk -g /dev/{self.parent.hddname}")
        self.parent.run_command(f"sgdisk -o /dev/{self.parent.hddname}")
        

        # =======================================================================================

        # MAKE BOOT PARTITION
        if os.path.isdir("/sys/firmware/efi/efivars"):
            self.parent.run_command(f"sgdisk -n 1:0:+256M -t 1:ef00 /dev/{self.parent.hddname}")
        else:
            self.parent.run_command(f"sgdisk -n 1:0:+5M -t 1:ef02 /dev/{self.parent.hddname}")

        # MAKE SWAP PARTITION
        self.parent.run_command(f"sgdisk -n 2:0:+{self.parent.swapsize}M -t 2:8200 /dev/{self.parent.hddname}")
        
        # MAKE ROOT PARTITION
        self.parent.run_command(f"sgdisk -n 3:0:0 -t 3:8300 /dev/{self.parent.hddname}")
        

        if "nvme" in self.parent.hddname:
            self.parent.hddname += "p"

    def prepare_hard_drive_ext4(self):

        self.auto_wipe_and_create()

        # Make file systems
        self.parent.run_command(f"mkfs.ext4 -F /dev/{self.parent.hddname}3")
        self.parent.run_command(f"mkswap /dev/{self.parent.hddname}2")
        
        # Mount Partitions
        # ============================================

        # mount root
        self.parent.run_command(f"mount -o rw,defaults,noatime /dev/{self.parent.hddname}3 /mnt")
        
        # mount boot
        if os.path.isdir("/sys/firmware/efi/efivars"):

            self.parent.run_command(f"mkfs.fat -F32 -n EFI /dev/{self.parent.hddname}1")
            self.parent.run_command(f"mkdir -p /mnt/boot/efi")
            self.parent.run_command(f"mount /dev/{self.parent.hddname}1 /mnt/boot/efi")


        # activate swap
        self.parent.run_command(f"swapon /dev/{self.parent.hddname}2")
        
        
        # generate fstab
        self.parent.run_command("mkdir -p /mnt/etc")
        with open("/mnt/etc/fstab", "w") as text_file:
            file = check_output(["genfstab", "-U", "-p", "/mnt"])
            for out in file:
                text_file.write(chr(out))

            text_file.truncate()

        if "nvme" in self.parent.hddname:
            self.parent.hddname = self.parent.hddname[:-1]

    def prepare_hard_drive_btrfs(self):

        self.auto_wipe_and_create()

        # make filesystems
        self.parent.run_command(f"mkfs.btrfs -f /dev/{self.parent.hddname}3")
        self.parent.run_command(f"mkswap /dev/{self.parent.hddname}2")
        
        # pre mount "ROOT" for subvol creation
        self.parent.run_command(f"mount -o rw,defaults,noatime /dev/{self.parent.hddname}3 /mnt")

        # Create subvolumes
        self.parent.run_command(f"btrfs subvolume create /mnt/@")

        for subvol in btrfs_subvolume_list:

            if subvol == ".snapshots":
                self.parent.run_command(f"btrfs subvolume create /mnt/@/{subvol}")
                self.parent.run_command(f"mkdir -p -v /mnt/@/.snapshots/1")
                self.parent.run_command(f"btrfs subvolume create /mnt/@/.snapshots/1/snapshot")

            elif subvol == "boot/grub2/i386-pc":
                self.parent.run_command(f"mkdir -p -v /mnt/@/boot/grub2/")
                self.parent.run_command(f"btrfs subvolume create /mnt/@/{subvol}")

            elif subvol == "usr/local":
                self.parent.run_command(f"mkdir -p -v /mnt/@/usr/")
                self.parent.run_command(f"btrfs subvolume create /mnt/@/{subvol}")

            else:
                self.parent.run_command(f"btrfs subvolume create /mnt/@/{subvol}")

        self.parent.run_command(f"chattr +C /mnt/@/var")

        # create snapshot 1
        with open("/mnt/@/.snapshots/1/info.xml", "w") as text_file:
            text_file.write('<?xml version="1.0"?>\n')
            text_file.write('<snapshot>\n')
            text_file.write('    <type>single</type>\n')
            text_file.write('    <num>1</num>\n')
            text_file.write('    <date>' + datetime.now().strftime('%Y-%m-%d %H:%M:%S') + '</date>\n')
            text_file.write('    <description>virgina</description>\n')
            text_file.write('</snapshot>\n')

        # get snapshot 1 subvolume id
        btrfs_out_str = check_output(["btrfs", "subvolume", "list", "/mnt"]).decode("utf-8").splitlines()
        id_out = ""
        for out in btrfs_out_str:
            if "snapshots/1/snapshot" in out:
                id_out = out.split()[1]

        # mount snapshot 1 for installation
        self.parent.run_command(f"btrfs subvolume set-default {id_out} /mnt")
        self.parent.run_command(f"umount /mnt")
        self.parent.run_command(f"mount -o rw,defaults,noatime /dev/{self.parent.hddname}3 /mnt")
    
        # prepare root tree
        for subvol in btrfs_subvolume_list:
            self.parent.run_command(f"mkdir -p -v /mnt/{subvol}")

        # mount subvolumes
        for subvol in btrfs_subvolume_list:
            self.parent.run_command(f"mount /dev/{self.parent.hddname}3 /mnt/{subvol} -o compress=zstd,subvol=@/{subvol}")

        # mount boot
        if os.path.isdir("/sys/firmware/efi/efivars"):
            self.parent.run_command(f"mkfs.fat -F32 -n EFI /dev/{self.parent.hddname}1")
            self.parent.run_command(f"mkdir -p /mnt/boot/efi")
            self.parent.run_command(f"mount /dev/{self.parent.hddname}1 /mnt/boot/efi")

        # activate swap
        self.parent.run_command(f"swapon /dev/{self.parent.hddname}2")

        # Generate fstab
        self.parent.run_command(f"mkdir -p /mnt/etc")
        with open("/mnt/etc/fstab", "w") as text_file:
            file = check_output(["genfstab", "-U", "-p", "/mnt"])
            for out in file:
                text_file.write(chr(out))

            text_file.truncate()

        # remove snapshot 1 from fstab
        with open("/mnt/etc/fstab", "r+") as text_file:
                file = text_file.readlines()
                text_file.seek(0)
                for out in file:
                    if '/@/.snapshots/1/snapshot' not in out:
                        text_file.write(out)
                text_file.truncate()

        # remove "p" fron nvme hdd name for grub legacy installation
        if "nvme" in self.parent.hddname:
            self.parent.hddname = self.parent.hddname[:-1]

        self.parent.run_command(
            "cp -f " +
            "/root/primearch/lib/resources/configs/btrfs/mkinitcpio.conf " +
            "/mnt/etc/mkinitcpio.conf"
        )
    
    def prepare_hard_drive_btrfs_minimal(self):

        self.auto_wipe_and_create()

        # make filesystems
        self.parent.run_command(f"mkfs.btrfs -f /dev/{self.parent.hddname}3")
        self.parent.run_command(f"mkswap /dev/{self.parent.hddname}2")
        
        # pre mount "ROOT" for subvol creation
        self.parent.run_command(f"mount -o rw,defaults,noatime /dev/{self.parent.hddname}3 /mnt")

        # Create subvolumes
        for subvol in btrfs_subvolume_list_minimal:
            self.parent.run_command(f"btrfs subvolume create /mnt/{subvol}")
        
        # unmount the harddrive for subvol mounting
        self.parent.run_command(f"umount /mnt")

        # mount subvolumes
        self.parent.run_command(f"mount /dev/{self.parent.hddname}3 /mnt/ -o compress=zstd,subvol=/@")
        self.parent.run_command(f"mkdir -p /mnt/home")
        self.parent.run_command(f"mkdir -p /mnt/var/log")
        self.parent.run_command(f"mkdir -p /mnt/var/cache/pacman/pkg")
        self.parent.run_command(f"mount /dev/{self.parent.hddname}3 /mnt/home -o compress=zstd,subvol=/@home")
        self.parent.run_command(f"mount /dev/{self.parent.hddname}3 /mnt/var/log -o compress=zstd,subvol=/@log")
        self.parent.run_command(f"mount /dev/{self.parent.hddname}3 /mnt/var/cache/pacman/pkg -o compress=zstd,subvol=/@pkg")
        
        # mount boot
        if os.path.isdir("/sys/firmware/efi/efivars"):
            self.parent.run_command(f"mkfs.fat -F32 -n EFI /dev/{self.parent.hddname}1")
            self.parent.run_command(f"mkdir -p /mnt/boot/efi")
            self.parent.run_command(f"mount /dev/{self.parent.hddname}1 /mnt/boot/efi")

        # activate swap
        self.parent.run_command(f"swapon /dev/{self.parent.hddname}2")

        # Generate fstab
        self.parent.run_command(f"mkdir -p /mnt/etc")
        with open("/mnt/etc/fstab", "w") as text_file:
            file = check_output(["genfstab", "-U", "-p", "/mnt"])
            for out in file:
                text_file.write(chr(out))

            text_file.truncate()

        # remove "p" fron nvme hdd name for grub legacy installation
        if "nvme" in self.parent.hddname:
            self.parent.hddname = self.parent.hddname[:-1]

    def prepare_hard_drive_manual(self):

        # # Make file systems
        #root
        self.parent.run_command(f"mkfs.ext4 -F /dev/{self.parent.partition_list[0]}")

        # home 
        if self.parent.partition_list[2] != "nothig mounted":
            self.parent.run_command(f"mkfs.ext4 -F /dev/{self.parent.partition_list[2]}")

        # swap
        if self.parent.partition_list[3] != "nothig mounted":
            self.parent.run_command(f"mkswap /dev/{self.parent.partition_list[3]}")


        # # Mount Partitions
        # root
        self.parent.run_command(f"mount -o rw,defaults,noatime /dev/{self.parent.partition_list[0]} /mnt")

        # boot
        if (
            self.parent.partition_list[1] != "nothig mounted" and
            os.path.isdir("/sys/firmware/efi/efivars")
        ):
            self.parent.run_command(f"mkfs.fat -F32 /dev/{self.parent.partition_list[1]}")
            self.parent.run_command(f"mkdir -p /mnt/boot/efi")
            self.parent.run_command(f"mount /dev/{self.parent.partition_list[1]} /mnt/boot/efi")

        # home
        if self.parent.partition_list[2] != "nothig mounted":
            self.parent.run_command(f"mkdir -p /mnt/home")
            self.parent.run_command(f"mount -o rw,defaults,noatime /dev/{self.parent.partition_list[2]} /mnt/home")

        # swap
        if self.parent.partition_list[3] != "nothig mounted":
            self.parent.run_command(f"swapon /dev/{self.parent.partition_list[3]}")


        # generate fstab
        self.parent.run_command("mkdir -p /mnt/etc")
        with open("/mnt/etc/fstab", "w") as text_file:
            file = check_output(["genfstab", "-U", "-p", "/mnt"])
            for out in file:
                text_file.write(chr(out))

            text_file.truncate()

        # set hard drive name for later use in "grub install", lagacy mode only
        if "nvme" in self.parent.partition_list[0]:
            self.parent.hddname = self.parent.partition_list[0][:-2]
        else:
            self.parent.hddname = self.parent.partition_list[0][:-1]


class Target_System:
    """
    class to work on the target system
    """
    def __init__(self):
        
        self.hostname = "ratArch"
        self.language = menu_list_language[0]
        self.sys_time = ["Europe", "Berlin", "utc"]
        self.desktop = menu_list_desktop[0]
        self.grafikdriver = menu_list_grafikdriver[0]

        self.users = [
            Target_User("root", "root", "disabled"),
            Target_User("user", "user", "enabled")
        ]
        
        self.hard_drive = Hard_drive(self)
        self.installmode = menu_list_installmode[2]
        self.swapsize = "1024"
        self.hddname = None
        self.partition_list = [
            "nothig mounted",
            "nothig mounted",
            "nothig mounted",
            "nothig mounted"
        ]
        
    def run_command(self, command):
        _ = run(
            command.split(" "),
            stdout=PIPE,
            stderr=PIPE
        )

        if _.stderr != None:
            with open("/root/error_log.txt", "a") as f:
                f.write(_.stderr.decode("utf-8") + "\n")

    def run_command_on_target(self, command):
        tmp = "arch-chroot /mnt /bin/bash -c".split(" ")
        tmp.append(command)
        _ = run(
            tmp,
            stdout=PIPE,
            stderr=PIPE
        )

        if _.stderr != None:
            with open("/root/error_log.txt", "a") as f:
                f.write(_.stderr.decode("utf-8") + "\n")
    
    def set_root(self):

        if self.users[0].status == "disabled":
            self.run_command_on_target("passwd -l root")

        else:
            pw_string = Popen(
                ["openssl", "passwd", "-1", "-salt", "xyz", self.users[0].password],
                universal_newlines=True,
                stdout=PIPE
            )

            ss1 = pw_string.communicate()[0].strip()

            with open("/mnt/etc/shadow", "r+") as text_file:
                file = text_file.readlines()
                text_file.seek(0)
                for out in file:
                    if "root" not in out:
                        text_file.write(out)
                text_file.truncate()
                text_file.write("\n" + "root:" + ss1 + ":17393::::::")

    def set_user(self):

        self.run_command_on_target(
            "useradd -m -g users -G wheel,storage,power,network,video,audio -s /usr/bin/fish " + self.users[1].name
        )
        
        pw_string = Popen(
            ["openssl", "passwd", "-1", "-salt", "xyz", self.users[1].password],
            universal_newlines=True,
            stdout=PIPE
        )

        ss1 = pw_string.communicate()[0].strip()

        with open("/mnt/etc/shadow", "r+") as text_file:
            file = text_file.readlines()
            text_file.seek(0)
            for out in file:
                if self.users[1].name not in out:
                    text_file.write(out)
            text_file.truncate()
            text_file.write("\n" + self.users[1].name + ":" + ss1 + ":17393:0:99999:7:::")
    
    def generate_locale(self):

        # set time zone and time
        self.run_command_on_target(
            "ln -sf /usr/share/zoneinfo/" +
            self.sys_time[0] + "/" + self.sys_time[1] +
            " /etc/localtime"
        )
        self.run_command_on_target("hwclock --systohc --" + self.sys_time[2])


        # generate language configs
        if self.language == "german":

            with open("/mnt/etc/locale.conf", "w") as text_file:
                text_file.write("LANG=de_DE.UTF-8" + "\n")
                text_file.write("LANGUAGE=de_DE" + "\n")

            with open("/mnt/etc/vconsole.conf", "w") as text_file:
                text_file.write("KEYMAP=de-latin1-nodeadkeys" + "\n")

            with open("/mnt/etc/locale.gen", "w") as text_file:
                text_file.write("de_DE.UTF-8 UTF-8" + "\n")

        elif self.language == "english":
            with open("/mnt/etc/locale.conf", "w") as text_file:
                text_file.write("LANG=en_US.UTF-8" + "\n")
                text_file.write("LANGUAGE=en_US.UTF-8" + "\n")

            with open("/mnt/etc/vconsole.conf", "w") as text_file:
                text_file.write("KEYMAP=en" + "\n")

            with open("/mnt/etc/locale.gen", "w") as text_file:
                text_file.write("en_US.UTF-8 UTF-8" + "\n")

        elif self.language == "russian":
            with open("/mnt/etc/locale.conf", "w") as text_file:
                text_file.write("LANG=ru_RU.UTF-8" + "\n")
                text_file.write("LANGUAGE=ru_RU.UTF-8" + "\n")

            with open("/mnt/etc/vconsole.conf", "w") as text_file:
                text_file.write("KEYMAP=ru" + "\n")

            with open("/mnt/etc/locale.gen", "w") as text_file:
                text_file.write("en_US.UTF-8 UTF-8" + "\n")
                text_file.write("ru_RU.UTF-8 UTF-8" + "\n")

        # generate_x_keyboard_config
        with open("/mnt/etc/X11/xorg.conf.d/00-keyboard.conf", "w") as text_file:
            text_file.write('Section "InputClass"')
            text_file.write('\n    Identifier "system-keyboard"')
            text_file.write('\n    MatchIsKeyboard "on"')

            if self.language == "german":
                text_file.write('\n    Option "XkbLayout" "de"')

            elif self.language == "english":
                text_file.write('\n    Option "XkbLayout" "en"')

            elif self.language == "russian":
                text_file.write('\n    Option "XkbLayout" "ru"')

            text_file.write('\nEndSection')


        # generate locale
        self.run_command_on_target("locale-gen")

    def install_cosmetic_stuff(self, return_queue):

        # Install yay pakages 
        # =======================================================================================
        return_queue.put("installing AUR pacage: yay")
        self.run_command(
            "cp -f " +
            "/root/primearch/lib/resources/aur_helper " +
            "/mnt/home/" + self.users[1].name + "/aur_helper"
        )

        self.run_command_on_target(
            "sudo -u " + self.users[1].name + " sh /home/" + self.users[1].name + "/aur_helper yay"
        )
            
        self.run_command("rm /mnt/home/" + self.users[1].name + "/aur_helper")
        # =======================================================================================


        # install yaru-gtk-theme
        # =======================================================================================
        with open("lib/pacages/pacages_aur", "r") as pkg:
            pacages_aur = pkg.read().splitlines()

        for x in pacages_aur:
            return_queue.put("installing AUR pacage: " + x.strip())
            self.run_command_on_target(
                "sudo -u " + self.users[1].name + " yay --noconfirm -S " + f"{x.strip()}"
            )
        # =======================================================================================
        


        # install configs for WinowManager
        # =======================================================================================
        return_queue.put("installing WM configs")

        if self.desktop == "i3":

            # Install custom I3 Configs
            self.run_command("rm -R /mnt/home/" + self.users[1].name + "/.config/i3")
            self.run_command(
                "git clone --recursive " +
                "https://gitlab.com/dikiy4eburator/i3-config.git " +
                "/mnt/home/" + self.users[1].name + "/.config/i3"
            )

            # Clone e_ascii engine
            self.run_command(
                "git clone " +
                "https://gitlab.com/dikiy4eburator/e_ascii.git " +
                "/mnt/usr/lib/python3.8/e_ascii"
            )

        if self.desktop == "qtile":

            # Install custom Qtile Configs
            self.run_command("rm -R /mnt/home/" + self.users[1].name + "/.config/qtile")
            self.run_command(
                "git clone " +
                "https://gitlab.com/dikiy4eburator/qtile-config.git " +
                "/mnt/home/" + self.users[1].name + "/.config/qtile"
            )
            
            # Copy my "stack" layout
            self.run_command(
                # make backup
                "mv -f -r " +
                "/mnt/usr/lib/python3.8/site-packages/libqtile/layout/stack.py " +
                "/mnt/usr/lib/python3.8/site-packages/libqtile/layout/stack_org.py"
            )
            self.run_command(
                "cp -f -r " +
                "/mnt/home/" + self.users[1].name + "/.config/qtile/scripts/stack_primearch.py " +
                "/mnt/usr/lib/python3.8/site-packages/libqtile/layout/stack.py"
            )

            # Clone e_ascii engine
            self.run_command(
                "git clone " +
                "https://gitlab.com/dikiy4eburator/e_ascii.git " +
                "/mnt/usr/lib/python3.9/e_ascii"
            )
        # =======================================================================================



        # General cosmetic stuff
        # =======================================================================================
        
        # Copy wallpapers
        return_queue.put("installing wallpapers")
        self.run_command(
            "cp -f -r " +
            "/root/primearch/lib/wallpapers/ " +
            "/mnt/usr/share/backgrounds/primearch/"
        )

        # Copy some interesting fonts
        return_queue.put("installing fonts")
        self.run_command("mkdir -p /mnt/home/" + self.users[1].name + "/.fonts")
        self.run_command(
            "cp -f -r " +
            "/root/.fonts " +
            "/mnt/home/" + self.users[1].name
        )

        # Copy lightdm config
        return_queue.put("installing lightdm configs")
        self.run_command(
            "cp -f -r " +
            "/root/primearch/lib/resources/configs/lightdm-gtk-greeter.conf " +
            "/mnt/etc/lightdm/lightdm-gtk-greeter.conf"
        )
        # =======================================================================================

    def install_pacages(self, return_queue):

        pac_files = [
            "pacages_base",
            "pacages_base_audio",
            "pacages_base_video",
            "pacages_base_network",
            "pacages_base_desktop",
            "pacages_base_cosmetik",
            "grafic_drivers/" + self.grafikdriver,
            "desktop/" + self.desktop
        ]

        # create pakage list
        pacages = list()

        for file in pac_files:
            with open("lib/pacages/" + file, "r") as pkg:
                for line in pkg:
                    if "#" in line or line.strip() == "":
                        pass
                    else:
                        pacages += line.splitlines()

        # INSTALL PACAGES
        for _pacage in pacages:
            return_queue.put("installing pacage: " + _pacage.strip())
            self.run_command("pacstrap /mnt " + _pacage)

    def install_system(self, engine, return_queue):

        # PREPARE HARD DRIVES
        # ===============================================================================
        return_queue.put("Preparing hard drives")

        if self.installmode == "auto_ext4":
            self.hard_drive.prepare_hard_drive_ext4()
        elif self.installmode == "auto_btrfs_suse_stile":
            self.hard_drive.prepare_hard_drive_btrfs()
        elif self.installmode == "auto_btrfs_minimal":
            self.hard_drive.prepare_hard_drive_btrfs_minimal()
        elif self.installmode == "manual":
            self.hard_drive.prepare_hard_drive_manual()



        # COPY pacman.conf file
        # ===============================================================================
        self.run_command(
            "cp -f " +
            "/root/primearch/lib/resources/configs/pacman.conf " +
            "/mnt/etc/pacman.conf"
        )


        # INSTALL PACAGES
        # ===============================================================================
        self.install_pacages(return_queue)


        # GO TO NO PASSWORD MODE
        # ===============================================================================
        with open("/mnt/etc/sudoers", "a") as text_file:
            text_file.write("\n" + "%wheel ALL=(ALL) NOPASSWD: ALL")


        # GENERATE SYSTEM LOCALE
        # ===============================================================================
        return_queue.put("geneating locale")
        self.generate_locale()


        # SET HOSTNAME
        # ===============================================================================
        return_queue.put("seting hostname")
        with open("/mnt/etc/hostname", "w") as text_file:
            text_file.write(self.hostname)


        # SET ROOT (if enabled)
        # ===============================================================================
        return_queue.put("seting ROOT")
        self.set_root()


        # CREATE NEW USER
        # ===============================================================================
        return_queue.put("seting USER")
        self.set_user()


        # INSTALL COSMETIC STUFF
        # ===============================================================================
        self.install_cosmetic_stuff(return_queue)


        # FIX USER HOME OWNERSHIP
        # ===============================================================================
        self.run_command_on_target(
            "chown -R " + self.users[1].name + ":users /home/" + self.users[1].name
        )
        self.run_command_on_target(
            "chmod -R 0700 /home/" + self.users[1].name
        )


        # START DAEMONS
        # ===============================================================================
        return_queue.put("Starting daemons")

        for daemon in daemons:
            self.run_command_on_target(daemon)
        
        if self.desktop == "gnome":
            self.run_command_on_target("systemctl enable gdm.service")
        else:
            self.run_command_on_target("systemctl enable lightdm.service")


        # GRUB INSTALLATION
        # ===============================================================================
        return_queue.put("installing grub")

        if os.path.isdir("/sys/firmware/efi/efivars"):
            self.run_command_on_target("grub-install --efi-directory=/boot/efi --bootloader-id=Arch_" + self.hostname)
        else:
            self.run_command_on_target("grub-install /dev/" + self.hddname)


        # install timeshift
        # =======================================================================================
        if self.installmode == "auto_btrfs_minimal":
            with open("lib/pacages/pacages_timeshift", "r") as pkg:
                pacages_aur = pkg.read().splitlines()

            for x in pacages_aur:
                return_queue.put("installing timeshift pacage: " + x.strip())
                self.run_command_on_target(
                    "sudo -u " + self.users[1].name + " yay --noconfirm -S " + f"{x.strip()}"
                )
        # =======================================================================================



        # ENABLE SNAPPER
        # ===============================================================================
        if self.installmode == "auto_btrfs":
            return_queue.put("preparing snapper")

            # install snap-pac-grub
            self.run_command_on_target(
                "sudo -u " + self.users[1].name +
                " gpg --recv-keys EB4F9E5A60D32232BB52150C12C87A28FEAC6B20"
            )
            self.run_command_on_target(
                "sudo -u " + self.users[1].name +
                " yay --noconfirm -S snap-pac-grub"
            )

            # copy snapper config file
            self.run_command(
                "cp -f " +
                "/mnt/etc/snapper/config-templates/default " +
                "/mnt/etc/snapper/configs/root"
            )

            # register snapper config file
            with open("/mnt/etc/conf.d/snapper", "r+") as text_file:
                file = text_file.readlines()

                text_file.seek(0)
                for out in file:
                    if 'SNAPPER_CONFIGS=""' in out:
                        text_file.write(out.replace('SNAPPER_CONFIGS=""', 'SNAPPER_CONFIGS="root"'))
                    else:
                        text_file.write(out)
                text_file.truncate()


        # COPY ACTUALIZED MIRRORLIST
        # ===============================================================================
        self.run_command("cp -f /etc/pacman.d/mirrorlist /mnt/etc/pacman.d/mirrorlist")
        

        # GO BACK TO  PASSWORD MODE
        # ===============================================================================
        with open("/mnt/etc/sudoers", "r+") as text_file:
            file = text_file.readlines()

            text_file.seek(0)
            for out in file:
                if "%wheel ALL=(ALL) NOPASSWD: ALL" in out:
                    text_file.write(out.replace("%wheel ALL=(ALL) NOPASSWD: ALL", "%wheel ALL=(ALL) ALL"))
                else:
                    text_file.write(out)
            text_file.truncate()


        # FINAL STEPS
        # ===============================================================================
        return_queue.put("FINAL STEPS")

        self.run_command_on_target("mkinitcpio -p linux")
        self.run_command_on_target("grub-mkconfig -o /boot/grub/grub.cfg")
        
        return_queue.put("DONE")
        return 0
