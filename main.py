# PrimeArch  Copyright (C) 2020  Wladimir Frank

import json
from time import sleep, time
from threading import Thread
from queue import Queue
from subprocess import run, check_output, PIPE

from lib.e_ascii import Grafik_engine

from lib.lib_objects import (
    Target_System,
    Hard_drive,
    menu_list_installmode,
    menu_list_sys_edit,
    menu_list_language,
    menu_list_time_format,
    menu_list_desktop,
    menu_list_grafikdriver
)

from lib.screans_general import (
    screan_wellcom,
    screan_summary,
    screan_end,
    screan_abort,
    screan_install_info
)

from lib.screans_hard_drive import (
    screan_select_harddrive,
    screan_manual_partition,
    screan_manual_mount,
)

from lib.screans_sys_edit import (
    screan_edit_sys_params,
    screan_set_hostname,
    screan_set_swap,
    screan_set_instal_mod,
    screan_set_languge,
    screan_select_desktop,
    screan_select_graphic_card,
    screan_set_time_zone,
    screan_set_time_subzone,
    screan_set_time_format
)

from lib.screans_user_edit import (
    screan_edit_user_name,
    screan_edit_user_password,
    screan_confirm_user_password,
    screan_password_missmatch
)


def input_loop(engine, target_system):

    # state_mashine init
    state_mashine = "wellcome"
    state_mashine_old = None
    state_mashine_last = None
    draw_screan = True

    while True:

        # THE STATE MASCHINE, THE ONE AND ONLY
        if state_mashine_old != state_mashine:

            # GENERAL SCREANS
            # ===========================================
            if state_mashine == "wellcome":
                screan_wellcom(
                    engine,
                    MENU_ORIGIN_Y, LINE, SCREAN_CENTER_X,
                    target_system,
                    version
                )
                state_mashine_old = state_mashine

            if state_mashine == "summary":
                screan_summary(
                    engine,
                    MENU_ORIGIN_Y, LINE, SCREAN_CENTER_X, MENU_OFFSET,
                    target_system,
                    version
                )
                state_mashine_old = state_mashine

            if state_mashine == "process_install":
                if not com_chanel_1.empty():
                    feedback = com_chanel_1.get()
                    if feedback == "DONE":
                        state_mashine_old = state_mashine
                        state_mashine = "end"
                    
                    screan_install_info(engine, feedback, LINE, SCREAN_CENTER_X, GOLDEN_CUT_Y, version)
            
            # ---------------- #

            if state_mashine == "end":
                screan_end(
                    engine,
                    MENU_ORIGIN_Y, LINE, SCREAN_CENTER_X,
                    target_system,
                    version
                )
                state_mashine_old = state_mashine

            if state_mashine == "abort":
                screan_abort(
                    engine,
                    MENU_ORIGIN_Y, LINE, SCREAN_CENTER_X,
                    target_system,
                    version
                )
                state_mashine_old = state_mashine



            # USER EDIT SCREANS
            # ===========================================
            if state_mashine == "edit_username":
                if draw_screan:
                    screan_edit_user_name(
                        engine,
                        LINE, SCREAN_CENTER_X, GOLDEN_CUT_Y,
                        version
                    )
                    draw_screan = False
                else:
                    for x in engine.draw_list:
                        if hasattr(x, 'state') and x.state == "DONE":
                            target_system.users[1].name = x.result

                            state_mashine_old = state_mashine
                            state_mashine = "edit_user_password"

                            draw_screan = True

                        elif hasattr(x, 'state') and x.state == "ABORT":
                            state_mashine_old = state_mashine
                            state_mashine = "summary"

                            draw_screan = True

                        continue

            if state_mashine == "edit_user_password":
                if draw_screan:
                    screan_edit_user_password(
                        engine,
                        LINE, SCREAN_CENTER_X, GOLDEN_CUT_Y,
                        target_system.users[1],
                        version
                    )
                    draw_screan = False
                else:
                    for x in engine.draw_list:
                        if hasattr(x, 'state') and x.state == "DONE":
                            target_system.users[1].password = x.result

                            state_mashine_old = state_mashine
                            state_mashine = "confirm_user_password"

                            draw_screan = True

                        elif hasattr(x, 'state') and x.state == "ABORT":
                            state_mashine_old = state_mashine
                            state_mashine = "summary"

                            draw_screan = True

                        continue

            if state_mashine == "confirm_user_password":
                if draw_screan:
                    screan_confirm_user_password(
                        engine,
                        LINE, SCREAN_CENTER_X, GOLDEN_CUT_Y,
                        target_system.users[1],
                        version
                    )
                    draw_screan = False
                else:
                    for x in engine.draw_list:
                        if hasattr(x, 'state') and x.state == "DONE":
                            if target_system.users[1].password == x.result:
                                state_mashine_old = state_mashine
                                state_mashine = "summary"
                                draw_screan = True
                            else:
                                screan_password_missmatch(
                                    engine,
                                    LINE, SCREAN_CENTER_X, GOLDEN_CUT_Y,
                                    version
                                )
                                state_mashine_old = state_mashine
                                state_mashine = "edit_user_password"
                                draw_screan = True

                        elif hasattr(x, 'state') and x.state == "ABORT":
                            target_system.users[1].password = "user"

                            state_mashine_old = state_mashine
                            state_mashine = "summary"

                            draw_screan = True

                        continue

            if state_mashine == "edit_root_password":
                if draw_screan:
                    screan_edit_user_password(
                        engine,
                        LINE, SCREAN_CENTER_X, GOLDEN_CUT_Y,
                        target_system.users[0],
                        version
                    )
                    draw_screan = False
                else:
                    for x in engine.draw_list:
                        if hasattr(x, 'state') and x.state == "DONE":
                            target_system.users[0].password = x.result

                            state_mashine_old = state_mashine
                            state_mashine = "confirm_root_password"

                            draw_screan = True

                        elif hasattr(x, 'state') and x.state == "ABORT":
                            state_mashine_old = state_mashine
                            state_mashine = "summary"

                            draw_screan = True

                        continue

            if state_mashine == "confirm_root_password":
                if draw_screan:
                    screan_confirm_user_password(
                        engine,
                        LINE, SCREAN_CENTER_X, GOLDEN_CUT_Y,
                        target_system.users[0],
                        version
                    )
                    draw_screan = False
                else:
                    for x in engine.draw_list:
                        if hasattr(x, 'state') and x.state == "DONE":
                            if target_system.users[0].password == x.result:

                                state_mashine_old = state_mashine
                                state_mashine = "summary"
                                draw_screan = True
                            else:
                                screan_password_missmatch(
                                    engine,
                                    LINE, SCREAN_CENTER_X, GOLDEN_CUT_Y,
                                    version
                                )
                                state_mashine_old = state_mashine
                                state_mashine = "edit_root_password"
                                draw_screan = True

                        elif hasattr(x, 'state') and x.state == "ABORT":
                            target_system.users[0].password = "root"

                            state_mashine_old = state_mashine
                            state_mashine = "summary"

                            draw_screan = True

                        continue



            # SYSTEM EDIT SCREANS
            # ===========================================
            if state_mashine == "edit_sys_param":
                if draw_screan:
                    screan_edit_sys_params(
                        engine,
                        LINE,
                        MENU_ORIGIN_Y,
                        SCREAN_CENTER_X,
                        MENU_OFFSET,
                        menu_list_sys_edit,
                        version
                    )
                    draw_screan = False
                else:
                    for x in engine.draw_list:
                        if hasattr(x, 'state') and x.state == "DONE":
                            ret = x.result

                            if ret == "Language":
                                state_mashine_old = state_mashine
                                state_mashine = "set_languge"
                                draw_screan = True

                            elif ret == "Time":
                                state_mashine_old = state_mashine
                                state_mashine = "set_time_zone"
                                draw_screan = True

                            elif ret == "Desktop":
                                state_mashine_old = state_mashine
                                state_mashine = "select_desktop"
                                draw_screan = True

                            elif ret == "Grafic Driver":
                                state_mashine_old = state_mashine
                                state_mashine = "select_grafic_card"
                                draw_screan = True

                            elif ret == "Install Mode":
                                state_mashine_old = state_mashine
                                state_mashine = "set_instal_mod"
                                draw_screan = True

                            elif ret == "Hostname":
                                state_mashine_old = state_mashine
                                state_mashine = "set_hostname"
                                draw_screan = True

                            elif ret == "SWAP size":
                                state_mashine_old = state_mashine
                                state_mashine = "set_swap_size"
                                draw_screan = True

                        elif hasattr(x, 'state') and x.state == "ABORT":
                            state_mashine_old = state_mashine
                            state_mashine = "summary"

                            draw_screan = True
                        continue

            # ---------------- #

            if state_mashine == "set_hostname":
                if draw_screan:
                    screan_set_hostname(
                        engine,
                        LINE, SCREAN_CENTER_X, GOLDEN_CUT_Y,
                        version
                    )
                    draw_screan = False
                else:
                    for x in engine.draw_list:
                        if hasattr(x, 'state') and x.state == "DONE":
                            target_system.hostname = x.result

                            state_mashine_old = state_mashine
                            state_mashine = "edit_sys_param"
                            draw_screan = True

                        elif hasattr(x, 'state') and x.state == "ABORT":
                            state_mashine_old = state_mashine

                            state_mashine = "edit_sys_param"
                            draw_screan = True

                        continue

            if state_mashine == "set_swap_size":
                if draw_screan:
                    screan_set_swap(
                        engine,
                        LINE, SCREAN_CENTER_X, GOLDEN_CUT_Y,
                        version
                    )
                    draw_screan = False
                else:
                    for x in engine.draw_list:
                        if hasattr(x, 'state') and x.state == "DONE":
                            target_system.swapsize = x.result

                            state_mashine_old = state_mashine
                            state_mashine = "edit_sys_param"
                            draw_screan = True

                        elif hasattr(x, 'state') and x.state == "ABORT":
                            state_mashine_old = state_mashine
                            state_mashine = "edit_sys_param"
                            draw_screan = True

                        continue

            # ---------------- #

            if state_mashine == "set_instal_mod":
                if draw_screan:
                    screan_set_instal_mod(
                        engine,
                        LINE, MENU_ORIGIN_Y, SCREAN_CENTER_X, MENU_OFFSET,
                        menu_list_installmode,
                        version
                    )
                    draw_screan = False
                else:
                    for x in engine.draw_list:
                        if hasattr(x, 'state') and x.state == "DONE":
                            target_system.installmode = x.result
                            state_mashine_old = state_mashine
                            state_mashine = "edit_sys_param"
                            draw_screan = True

                        elif hasattr(x, 'state') and x.state == "ABORT":
                            state_mashine_old = state_mashine
                            state_mashine = "edit_sys_param"
                            draw_screan = True

                        continue

            if state_mashine == "set_languge":
                if draw_screan:
                    screan_set_languge(
                        engine,
                        LINE, MENU_ORIGIN_Y,
                        SCREAN_CENTER_X, MENU_OFFSET,
                        menu_list_language,
                        version
                    )
                    draw_screan = False
                else:
                    for x in engine.draw_list:
                        if hasattr(x, 'state') and x.state == "DONE":
                            target_system.language = x.result
                            state_mashine_old = state_mashine
                            state_mashine = "edit_sys_param"
                            draw_screan = True

                        elif hasattr(x, 'state') and x.state == "ABORT":
                            state_mashine_old = state_mashine
                            state_mashine = "edit_sys_param"
                            draw_screan = True

                        continue

            if state_mashine == "select_desktop":
                if draw_screan:
                    screan_select_desktop(
                        engine,
                        LINE, MENU_ORIGIN_Y, SCREAN_CENTER_X, MENU_OFFSET,
                        menu_list_desktop,
                        version
                    )
                    draw_screan = False
                else:
                    for x in engine.draw_list:
                        if hasattr(x, 'state') and x.state == "DONE":
                            target_system.desktop = x.result
                            state_mashine_old = state_mashine
                            state_mashine = "edit_sys_param"
                            draw_screan = True

                        elif hasattr(x, 'state') and x.state == "ABORT":
                            state_mashine_old = state_mashine
                            state_mashine = "edit_sys_param"
                            draw_screan = True

                        continue

            if state_mashine == "select_grafic_card":
                if draw_screan:
                    screan_select_graphic_card(
                        engine,
                        LINE, MENU_ORIGIN_Y, SCREAN_CENTER_X, MENU_OFFSET,
                        menu_list_grafikdriver,
                        version
                    )
                    draw_screan = False
                else:
                    for x in engine.draw_list:
                        if hasattr(x, 'state') and x.state == "DONE":
                            target_system.grafikdriver = x.result
                            state_mashine_old = state_mashine
                            state_mashine = "edit_sys_param"
                            draw_screan = True

                        elif hasattr(x, 'state') and x.state == "ABORT":
                            state_mashine_old = state_mashine
                            state_mashine = "edit_sys_param"
                            draw_screan = True

                        continue

            if state_mashine == "select_harddrive":
                if draw_screan:
                    screan_select_harddrive(
                        engine,
                        LINE, MENU_ORIGIN_Y, SCREAN_CENTER_X, MENU_OFFSET,
                        target_system,
                        version
                    )
                    draw_screan = False
                else:
                    for x in engine.draw_list:
                        if hasattr(x, 'state') and x.state == "DONE":
                            target_system.hddname = x.result
                            state_mashine_old = state_mashine
                            state_mashine = "summary"
                            draw_screan = True

                        elif hasattr(x, 'state') and x.state == "ABORT":
                            state_mashine_old = state_mashine
                            state_mashine = "summary"
                            draw_screan = True

                        continue

            if state_mashine == "set_time_zone":
                if draw_screan:
                    with open('lib/resources/time.json') as json_file:
                        time_zone = json.load(json_file)

                    zone_menu_list = list()
                    for i, x in enumerate(time_zone):
                        zone_menu_list.append(f"{time_zone[i]['group']}")

                    screan_set_time_zone(
                        engine,
                        LINE, MENU_ORIGIN_Y, SCREAN_CENTER_X, MENU_OFFSET,
                        zone_menu_list,
                        version
                    )
                    draw_screan = False
                else:
                    for x in engine.draw_list:
                        if hasattr(x, 'state') and x.state == "DONE":
                            target_system.sys_time[0] = x.result
                            state_mashine_old = state_mashine
                            state_mashine = "set_time_subzone"
                            draw_screan = True

                        elif hasattr(x, 'state') and x.state == "ABORT":
                            state_mashine_old = state_mashine
                            state_mashine = "edit_sys_param"
                            draw_screan = True

                        continue

            if state_mashine == "set_time_subzone":
                if draw_screan:
                    time_zone_index = int()
                    with open('lib/resources/time.json') as json_file:
                        time_zone = json.load(json_file)

                    sub_zone_menu_list = list()
                    for i, x in enumerate(time_zone):
                        if target_system.sys_time[0] in x['group']:
                            time_zone_index = i

                    for i, x in enumerate(time_zone[time_zone_index]['zones']):
                        sub_zone_menu_list.append(f"{time_zone[time_zone_index]['zones'][i]['name']}")

                    screan_set_time_subzone(
                        engine,
                        LINE, MENU_ORIGIN_Y, SCREAN_CENTER_X, MENU_OFFSET,
                        sub_zone_menu_list,
                        version
                    )
                    draw_screan = False
                else:
                    for x in engine.draw_list:
                        if hasattr(x, 'state') and x.state == "DONE":
                            target_system.sys_time[1] = x.result
                            state_mashine_old = state_mashine
                            state_mashine = "set_time_format"
                            draw_screan = True

                        elif hasattr(x, 'state') and x.state == "ABORT":
                            state_mashine_old = state_mashine
                            state_mashine = "edit_sys_param"
                            draw_screan = True

                        continue

            if state_mashine == "set_time_format":
                if draw_screan:
                    screan_set_time_format(
                        engine,
                        LINE, MENU_ORIGIN_Y, SCREAN_CENTER_X, MENU_OFFSET,
                        menu_list_time_format,
                        version
                    )
                    draw_screan = False
                else:
                    for x in engine.draw_list:
                        if hasattr(x, 'state') and x.state == "DONE":
                            target_system.sys_time[2] = x.result
                            state_mashine_old = state_mashine
                            state_mashine = "edit_sys_param"
                            draw_screan = True

                        elif hasattr(x, 'state') and x.state == "ABORT":
                            state_mashine_old = state_mashine
                            state_mashine = "edit_sys_param"
                            draw_screan = True

                        continue



            # MANUAL MOUNT SREANS
            # ===========================================
            if state_mashine == "manual_mount":
                if draw_screan:
                    screan_manual_partition(
                        engine,
                        MENU_ORIGIN_Y, LINE, SCREAN_CENTER_X, MENU_OFFSET,
                        target_system,
                        version
                    )
                    draw_screan = False
                else:
                    for x in engine.draw_list:
                        if hasattr(x, 'state') and x.state == "DONE":
                            state_mashine_old = state_mashine
                            state_mashine = x.result
                            draw_screan = True

                        elif hasattr(x, 'state') and x.state == "ABORT":
                            state_mashine_old = state_mashine
                            state_mashine = "summary"
                            draw_screan = True

                        continue

            # ---------------- #

            if state_mashine == "Mount ROOT":
                if draw_screan:
                    screan_manual_mount(
                        engine,
                        MENU_ORIGIN_Y, LINE, SCREAN_CENTER_X, MENU_OFFSET,
                        target_system,
                        version
                    )
                    draw_screan = False
                else:
                    for x in engine.draw_list:
                        if hasattr(x, 'state') and x.state == "DONE":
                            target_system.partition_list[0] = x.result
                            state_mashine_old = state_mashine
                            state_mashine = "manual_mount"
                            draw_screan = True

                        elif hasattr(x, 'state') and x.state == "ABORT":
                            state_mashine_old = state_mashine
                            state_mashine = "manual_mount"
                            draw_screan = True

                        continue

            if state_mashine == "Mount BOOT":
                if draw_screan:
                    screan_manual_mount(
                        engine,
                        MENU_ORIGIN_Y, LINE, SCREAN_CENTER_X, MENU_OFFSET,
                        target_system,
                        version
                    )
                    draw_screan = False
                else:
                    for x in engine.draw_list:
                        if hasattr(x, 'state') and x.state == "DONE":
                            target_system.partition_list[1] = x.result
                            state_mashine_old = state_mashine
                            state_mashine = "manual_mount"
                            draw_screan = True

                        elif hasattr(x, 'state') and x.state == "ABORT":
                            state_mashine_old = state_mashine
                            state_mashine = "manual_mount"
                            draw_screan = True

                        continue

            if state_mashine == "Mount HOME":
                if draw_screan:
                    screan_manual_mount(
                        engine,
                        MENU_ORIGIN_Y, LINE, SCREAN_CENTER_X, MENU_OFFSET,
                        target_system,
                        version
                    )
                    draw_screan = False
                else:
                    for x in engine.draw_list:
                        if hasattr(x, 'state') and x.state == "DONE":
                            target_system.partition_list[2] = x.result
                            state_mashine_old = state_mashine
                            state_mashine = "manual_mount"
                            draw_screan = True

                        elif hasattr(x, 'state') and x.state == "ABORT":
                            state_mashine_old = state_mashine
                            state_mashine = "manual_mount"
                            draw_screan = True

                        continue

            if state_mashine == "Mount SWAP":
                if draw_screan:
                    screan_manual_mount(
                        engine,
                        MENU_ORIGIN_Y, LINE, SCREAN_CENTER_X, MENU_OFFSET,
                        target_system,
                        version
                    )
                    draw_screan = False
                else:
                    for x in engine.draw_list:
                        if hasattr(x, 'state') and x.state == "DONE":
                            target_system.partition_list[3] = x.result
                            state_mashine_old = state_mashine
                            state_mashine = "manual_mount"
                            draw_screan = True

                        elif hasattr(x, 'state') and x.state == "ABORT":
                            state_mashine_old = state_mashine
                            state_mashine = "manual_mount"
                            draw_screan = True

                        continue


        # INPUT BLOCK
        if not engine.key_buffer.empty():
            input_main_loop = engine.key_buffer.get()
            
            if state_mashine == "wellcome":
                if input_main_loop == "KEY_ENTER":
                    state_mashine = "summary"

                elif input_main_loop == "KEY_ESCAPE":
                    state_mashine_last = state_mashine
                    state_mashine = "abort"

            elif state_mashine == "summary":
                if input_main_loop == "0":
                    if (target_system.installmode == "auto_ext4" or
                        target_system.installmode == "auto_btrfs_suse_stile" or
                        target_system.installmode == "auto_btrfs_minimal"
                        ):
                        
                        if target_system.hddname is not None:
                            state_mashine = "process_install"
                            install_thread = Thread(
                                target=target_system.install_system,
                                args=(
                                    engine,
                                    com_chanel_1,
                                )
                            )
                            install_thread.daemon = True
                            install_thread.start()

                    elif target_system.installmode == "manual":
                        if target_system.partition_list[0] != "nothig mounted":
                            state_mashine = "process_install"
                            install_thread = Thread(
                                target=target_system.install_system,
                                args=(
                                    engine,
                                    com_chanel_1,
                                )
                            )
                            install_thread.daemon = True
                            install_thread.start()
                    
                elif input_main_loop == "1":
                    state_mashine = "edit_username"

                elif input_main_loop == "2":
                    if target_system.users[0].status == "disabled":
                        target_system.users[0].status = "enabled"
                        if target_system.users[0].password == "root":
                            state_mashine = "edit_root_password"

                    elif target_system.users[0].status == "enabled":
                        target_system.users[0].status = "disabled"

                    state_mashine_old = "x"

                elif input_main_loop == "3":
                    state_mashine = "edit_sys_param"

                elif input_main_loop == "4":
                    if target_system.installmode == "manual":
                        state_mashine = "manual_mount"
                    else:
                        state_mashine = "select_harddrive"

                elif input_main_loop == "KEY_ESCAPE":
                    state_mashine_last = state_mashine
                    state_mashine = "abort"

            elif state_mashine == "end":
                if input_main_loop == "r":
                    run("umount -R /mnt".split())
                    run("systemctl reboot".split())
                    engine.main_loop_run = False

                elif input_main_loop == "s":
                    engine.main_loop_run = False

            elif state_mashine == "abort":
                if input_main_loop == "y":
                    engine.main_loop_run = False
                    engine.clear_frame_buffer()
                    exit(0)

                elif input_main_loop == "n":
                    state_mashine = state_mashine_last
                    state_mashine_old = "x"

        sleep(0.01)


if __name__ == "__main__":

    print("\n" * 50)

    # create grafik engine objekt
    engine = Grafik_engine()

    # create system objekt
    target_system = Target_System()
    
    # Define some random stuff
    com_chanel_1 = Queue()

    # get branch name
    version = [str(), "4.6"]
    branch_list = run("git branch".split(), stderr=PIPE, stdout=PIPE)
    for branch in branch_list.stdout.decode("utf-8").split("\n"):
        if "*" in branch:
            version[0] = branch.split()[1]
    
    # Style markers
    SCREAN_CENTER_X = int(engine.canvas_x[1] / 2)
    MENU_OFFSET = 1
    LINE = (engine.canvas_y[0] + 1, engine.canvas_y[1] - 2)
    GOLDEN_CUT_Y = int(engine.canvas_y[1] - engine.canvas_y[1] / 1.618)
    MENU_ORIGIN_Y = engine.canvas_y[1] - 7

    # start input/control loop
    input_thread = Thread(target=input_loop, args=(engine, target_system,))
    input_thread.daemon = True
    input_thread.start()
    
    # start engine main loop
    engine.main_loop()
